package main

import (
	"bytes"
	"fmt"
	"image"
	_ "image/png"
	"io/ioutil"

	"bitbucket.org/smart_beehive/backend/internal/tools/encrypt"
	"github.com/liyue201/goqr"
	"github.com/spf13/cobra"
)

var (
	privateKey string
)

func main() {
	cmd := &cobra.Command{
		Use:   "qr-code-decoder",
		Short: "Декодер содержимого QR кода.",
		Run:   decode,
	}

	cmd.PersistentFlags().StringVar(
		&privateKey,
		"private-key",
		"",
		"Приватный ключ для шифрования идентификаторов.",
	)

	cobra.CheckErr(cmd.Execute())
}

func decode(cmd *cobra.Command, args []string) {
	if len(args) == 0 {
		fmt.Println("Файл QR кода не передан.")
		return
	}

	for _, file := range args {
		fmt.Println(file)

		imgdata, err := ioutil.ReadFile(file)
		if err != nil {
			fmt.Printf("%v\n", err)
			return
		}

		img, _, err := image.Decode(bytes.NewReader(imgdata))
		if err != nil {
			fmt.Printf("image.Decode error: %v\n", err)
			return
		}

		qrCodes, err := goqr.Recognize(img)
		if err != nil {
			fmt.Printf("Recognize failed: %v\n", err)
			return
		}

		fmt.Println("Содержимое:")

		encryptText := string(qrCodes[0].Payload)
		fmt.Println(encryptText)

		fmt.Println("Расшифровка:")
		plaintext, err := encrypt.Decrypt(encryptText, privateKey)
		if err != nil {
			fmt.Printf("Decode failed: %v\n", err)
			return
		}

		fmt.Println(plaintext)
		fmt.Println()
	}
}
