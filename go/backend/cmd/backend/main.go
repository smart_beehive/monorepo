package main

import (
	"context"
	"fmt"
	"os"

	"bitbucket.org/smart_beehive/backend/internal"
	endpoints "bitbucket.org/smart_beehive/backend/internal/endpoints/http"
	mwsauth "bitbucket.org/smart_beehive/backend/internal/mws/auth"
	mwslogging "bitbucket.org/smart_beehive/backend/internal/mws/logging"
	mwstx "bitbucket.org/smart_beehive/backend/internal/mws/tx"
	"bitbucket.org/smart_beehive/backend/internal/repositories/pg"
	servers "bitbucket.org/smart_beehive/backend/internal/servers/http"
	"bitbucket.org/smart_beehive/backend/internal/services"
	"bitbucket.org/smart_beehive/backend/internal/tools/config"
	"bitbucket.org/smart_beehive/backend/internal/tools/gfs"
	"bitbucket.org/smart_beehive/backend/internal/tools/log"
	"bitbucket.org/smart_beehive/backend/internal/tools/proc"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/cors"
	"github.com/spf13/cobra"
)

func main() {
	rootCmd := &cobra.Command{
		Use:   "backend",
		Short: "Монолитный бэкенд приложения 'Умный улей'",
	}

	configCmd := &cobra.Command{
		Use:   "config",
		Short: "Вывести таблицу с описанием переменных окружения.",
		Run:   printConfig,
	}

	initCmd := &cobra.Command{
		Use:   "init",
		Short: "Инициализация конфига переменных окружения рядом с бинарником",
		Run:   initConfig,
	}

	serveCmd := &cobra.Command{
		Use:   "serve",
		Short: "Запускает API сервер",
		Run:   serve,
	}

	rootCmd.AddCommand(
		configCmd,
		initCmd,
		serveCmd,
	)

	cobra.CheckErr(rootCmd.Execute())
}

type Config struct {
	PGConnectionString string `required:"true" split_words:"true" desc:"Строка подключения к серверу/кластеру postgres."`

	GoogleOAuth      services.OAuthCredentionals `split_words:"true"`
	RedirectOAuthURL string                      `required:"true" split_words:"true" desc:"url куда oauth провайдер выполнит редирект."`

	AuthService      services.AuthServiceConfig `split_words:"true"`
	QRCodePrivateKey string                     `required:"true" split_words:"true" desc:"приватный ключ для расшифровки содержимого QR кодов."`

	Server servers.ServerConfig `split_words:"true"`
}

func printConfig(cmd *cobra.Command, args []string) {
	err := config.WriteConfigInfoToStdout(&Config{})
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
	}
}

func initConfig(cmd *cobra.Command, args []string) {
	err := config.InitConfigFile(&Config{})
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
	}
}

func serve(cmd *cobra.Command, args []string) {
	l := log.MakeLogger()

	conf := new(Config)

	err := config.LoadConfig(conf)
	if err != nil {
		panic(fmt.Errorf("LoadConfig: %w", err))
	}

	pgpool, err := pgxpool.Connect(context.Background(), conf.PGConnectionString)
	if err != nil {
		panic(fmt.Errorf("pgxpool.Connect: %w", err))
	}
	defer pgpool.Close()

	transactor := pg.MakeTransactor(pgpool, l)
	authTokens := pg.NewAuthTokens(pgpool)
	devices := pg.NewDevices(pgpool)
	exchangeCodeForTokens := pg.NewExchangeCodeForTokens(pgpool)
	oauthConnectProviderSessions := pg.NewOAuthConnectProviderSessions(pgpool)
	users := pg.NewUsers(pgpool)
	externalUsers := pg.NewExternalUsers(pgpool)
	apiaries := pg.NewApiaries(pgpool)
	bindedApiaries := pg.NewBindedApiaries(pgpool, 15)
	beehives := pg.NewBeeHives(pgpool)
	bindedBeehives := pg.NewBindedBeeHives(pgpool, 15)

	googleOAuthProvider := services.NewGoogleOAuthProvider(conf.RedirectOAuthURL, conf.GoogleOAuth)

	authService := internal.WrapAuthServiceMW(
		services.NewAuthService(
			oauthConnectProviderSessions,
			externalUsers,
			users,
			exchangeCodeForTokens,
			devices,
			authTokens,
			[]services.OAuthProvider{googleOAuthProvider},
			conf.AuthService,
		),
		mwstx.MakeForAuthService(transactor),
		mwsauth.MakeForAuthService(),
		mwslogging.MakeForAuthService(l),
	)

	accountApiariesService := services.NewAccountApiariesService(apiaries, bindedApiaries, bindedBeehives, conf.QRCodePrivateKey)
	accountBeehivesService := services.NewAccountBeeHivesService(beehives, bindedBeehives, bindedApiaries, conf.QRCodePrivateKey)

	requestReader := endpoints.WrapRequestReader(endpoints.ReadJSON, mwslogging.MakeForRequestReader(l))
	responseWriter := endpoints.WrapResponseWriter(endpoints.WriteJSON, mwslogging.MakeForResponseWriter(l))
	endpointsBase := endpoints.NewEndpointsBase(requestReader, responseWriter)

	authServiceEndpoints := endpoints.NewAuthServiceEndpoints(endpointsBase, authService)
	accountApiariesServiceEndpoints := endpoints.NewAccountApiariesServiceEndpoints(endpointsBase, accountApiariesService)
	accountBeeHiveServicesEndpoints := endpoints.NewAccountBeeHiveServiceEndpoints(endpointsBase, accountBeehivesService)

	server := servers.NewServer(conf.Server)

	servers.RegistrationAuthServiceEndpoints(server, authServiceEndpoints)
	servers.RegistrationAccountApiariesServiceEndpoints(server, accountApiariesServiceEndpoints)
	servers.RegistrationAccountBeeHiveServiceEndpoints(server, accountBeeHiveServicesEndpoints)

	server.UseMWS(
		cors.AllowAll().Handler,
		endpoints.MakeLogging(l),
		endpoints.MakePassingClientClaimsToRequestContext(),
		endpoints.MakeAuthTokenToRequestContext(authTokens.ReadByAccessToken, "Bearer"),
	)

	if err = server.LogRouter(l); err != nil {
		panic(fmt.Errorf("server.LogRouter: %w", err))
	}

	err = proc.ParallelJobs(
		context.Background(),
		gfs.SignalNotify,
		server.ListenAndServe,
	)
	if err != nil && !gfs.IsErrOSSignal(err) {
		panic(fmt.Errorf("proc.ParallelJobs: %w", err))
	}

	l.Info("stop app")
}
