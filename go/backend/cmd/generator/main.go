package main

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"bitbucket.org/smart_beehive/backend/internal/repositories"
	"bitbucket.org/smart_beehive/backend/internal/repositories/pg"
	"bitbucket.org/smart_beehive/backend/internal/tools/encrypt"
	"bitbucket.org/smart_beehive/backend/internal/tools/log"
	"github.com/jackc/pgx/v4"
	"github.com/skip2/go-qrcode"
	"github.com/spf13/cobra"
)

var (
	database            string
	count               int
	apiariesDestination string
	beehivesDestination string
	_privateKey         string
)

func main() {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}

	smartbeehiveDir := filepath.Join(homeDir, ".smartbeehive")
	defaultApiaries := filepath.Join(smartbeehiveDir, "apiaries")
	defaultBeehives := filepath.Join(smartbeehiveDir, "beehives")

	root := &cobra.Command{
		Use:   "generator",
		Short: "Утилита для генерирования сущностей ульев, пасек, приватных ключей.",
	}

	privateKey := &cobra.Command{
		Use:   "private-key",
		Short: "Генератор 32-х разрядного HEX ключа.",
		Run:   privateKey,
	}

	apiary := &cobra.Command{
		Use:   "apiary",
		Short: "Сгенерировать пасеки",
		Run:   apiary,
	}

	apiary.PersistentFlags().StringVar(
		&database,
		"database",
		"host=localhost port=5432 user=pguser password=password database=smartbeehive sslmode=disable",
		"Строка подключения к базеданных.",
	)

	apiary.PersistentFlags().IntVar(
		&count,
		"count",
		1,
		"Кол-во пасек которое надо сгенерировать.",
	)

	apiary.PersistentFlags().StringVar(
		&apiariesDestination,
		"destination",
		defaultApiaries,
		"Директория куда будут помещены QR коды",
	)

	apiary.PersistentFlags().StringVar(
		&_privateKey,
		"private-key",
		"",
		"Приватный ключ для шифрования идентификаторов",
	)

	beehive := &cobra.Command{
		Use:   "beehive",
		Short: "Сгенерировать ульи",
		Run:   beehive,
	}

	beehive.PersistentFlags().StringVar(
		&database,
		"database",
		"host=localhost port=5432 user=pguser password=password database=smartbeehive sslmode=disable",
		"Строка подключения к базеданных.",
	)

	beehive.PersistentFlags().IntVar(
		&count,
		"count",
		1,
		"Кол-во пасек которое надо сгенерировать.",
	)

	beehive.PersistentFlags().StringVar(
		&beehivesDestination,
		"destination",
		defaultBeehives,
		"Директория куда будут помещены QR коды.",
	)

	beehive.PersistentFlags().StringVar(
		&_privateKey,
		"private-key",
		"",
		"Приватный ключ для шифрования идентификаторов.",
	)

	root.AddCommand(
		privateKey,
		apiary,
		beehive,
	)

	cobra.CheckErr(root.Execute())
}

func privateKey(cmd *cobra.Command, args []string) {
	bytes := make([]byte, 32) //generate a random 32 byte key for AES-256
	if _, err := rand.Read(bytes); err != nil {
		fmt.Println(err)

		return
	}

	key := hex.EncodeToString(bytes) //encode key in bytes to string and keep as secret, put in a vault

	fmt.Println(key)
}

func apiary(cmd *cobra.Command, args []string) {
	ctx := cmd.Context()
	l := log.MakeLogger()

	pgconn, err := pgx.Connect(ctx, database)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer pgconn.Close(context.Background())

	transactor := pg.MakeTransactor(pgconn, l)

	err = os.MkdirAll(apiariesDestination, 0700)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	apiariesRepo := repositories.Apiaries(pg.NewApiaries(pgconn))

	err = transactor(ctx, func(txCtx context.Context) error {
		for i := 0; i < count; i++ {
			apiary, err := apiariesRepo.Create(ctx)
			if err != nil {
				return err
			}

			encyptedID, err := encrypt.Encrypt(apiary.ID, _privateKey)
			if err != nil {
				return err
			}

			qrCodeContentPath := filepath.Join(apiariesDestination, fmt.Sprintf("%s.txt", apiary.ID))
			qrCodePath := filepath.Join(apiariesDestination, fmt.Sprintf("%s.png", apiary.ID))

			err = ioutil.WriteFile(qrCodeContentPath, []byte(encyptedID), 0700)
			if err != nil {
				return err
			}

			err = qrcode.WriteFile(
				encyptedID,
				qrcode.Medium,
				256,
				qrCodePath,
			)
			if err != nil {
				return err
			}

			fmt.Println(qrCodeContentPath)
			fmt.Println(qrCodePath)
		}

		return nil
	})
	if err != nil {
		fmt.Println(err.Error())
	}
}

func beehive(cmd *cobra.Command, args []string) {
	ctx := cmd.Context()
	l := log.MakeLogger()

	pgconn, err := pgx.Connect(ctx, database)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer pgconn.Close(context.Background())

	transactor := pg.MakeTransactor(pgconn, l)

	err = os.MkdirAll(beehivesDestination, 0700)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	beehivesRepo := repositories.BeeHives(pg.NewBeeHives(pgconn))

	err = transactor(ctx, func(txCtx context.Context) error {
		for i := 0; i < count; i++ {
			beehive, err := beehivesRepo.Create(ctx)
			if err != nil {
				return err
			}

			encyptedID, err := encrypt.Encrypt(beehive.ID, _privateKey)
			if err != nil {
				return err
			}

			qrCodeContentPath := filepath.Join(apiariesDestination, fmt.Sprintf("%s.txt", beehive.ID))
			qrCodePath := filepath.Join(beehivesDestination, fmt.Sprintf("%s.png", beehive.ID))

			err = ioutil.WriteFile(qrCodeContentPath, []byte(encyptedID), 0700)
			if err != nil {
				return err
			}

			err = qrcode.WriteFile(
				encyptedID,
				qrcode.Medium,
				256,
				qrCodePath,
			)
			if err != nil {
				return err
			}

			fmt.Println(qrCodeContentPath)
			fmt.Println(qrCodePath)
		}

		return nil
	})
	if err != nil {
		fmt.Println(err.Error())
	}
}
