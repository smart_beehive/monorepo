package models

import (
	"context"
	"time"
)

type AuthToken struct {
	UID                 string     `json:"uid"`
	DID                 string     `json:"did"`
	AccessToken         string     `json:"access_token"`
	RefreshToken        string     `json:"refresh_token"`
	AccessExpiredAfter  time.Time  `json:"expired_after"`
	RefreshExpiredAfter time.Time  `json:"refresh_expired_after"`
	IsEnabled           bool       `json:"is_enabled"`
	CreatedAt           time.Time  `json:"created_at"`
	DisabledAt          *time.Time `json:"disabled_at"`
}

func (t *AuthToken) IsAccessTokenValid() bool {
	return t.IsEnabled && time.Now().Before(t.AccessExpiredAfter)
}

func (t *AuthToken) IsRefreshTokenValid() bool {
	return t.IsEnabled && time.Now().Before(t.RefreshExpiredAfter)
}

type authTokenCtxKey struct{}

func (t *AuthToken) ToCtx(parent context.Context) context.Context {
	return context.WithValue(parent, authTokenCtxKey{}, *t)
}

func AuthTokenFromContext(ctx context.Context) (authToken AuthToken, isExists bool) {
	authToken, isExists = ctx.Value(authTokenCtxKey{}).(AuthToken)

	return authToken, isExists
}

func MustAuthTokenFromContext(ctx context.Context) (authToken AuthToken) {
	authToken, isExists := AuthTokenFromContext(ctx)
	if !isExists {
		panic("AuthToken does not found in ctx")
	}

	return authToken
}
