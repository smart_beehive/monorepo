package models

import "time"

type BindedBeeHive struct {
	ID           string     `json:"id"`
	BindApiaryID string     `json:"bind_apiary_id"`
	BeeHiveID    string     `json:"beehive_id"`
	CreatedAt    time.Time  `json:"created_at"`
	UpdatedAt    *time.Time `json:"updated_at,omitempty"`
	DeletedAt    *time.Time `json:"deleted_at,omitempty"`
}
