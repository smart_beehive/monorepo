package models

import (
	"time"
)

type OAuthConnectProviderSession struct {
	ID           string            `json:"id"`
	ClientClaims ClientClaims      `json:"client_claims"`
	ProviderName OAuthProviderName `json:"provider_name"`
	StartedAt    time.Time         `json:"started_at"`
	ExpiredAfter time.Time         `json:"expired_after"`
}
