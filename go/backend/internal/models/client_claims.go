package models

import (
	"context"
	"errors"
)

type ClientClaims struct {
	IP        string `json:"ip"`
	UserAgent string `json:"user_agent"`
}

var ErrIPAndUserAgentMustNotBeEmpty = errors.New("ip and user-agent must be not empty")

func NewClientClaims(ip string, userAgent string) (*ClientClaims, error) {
	if ip == "" || userAgent == "" {
		return nil, ErrIPAndUserAgentMustNotBeEmpty
	}

	return &ClientClaims{
		IP:        ip,
		UserAgent: userAgent,
	}, nil
}

type clientClaimsCtxKey struct{}

func ClientClaimsFromCtx(ctx context.Context) (claims ClientClaims, isExists bool) {
	claims, isExists = ctx.Value(clientClaimsCtxKey{}).(ClientClaims)

	return claims, isExists
}

func MustClientClaimsFromCtx(ctx context.Context) ClientClaims {
	claims, isExists := ClientClaimsFromCtx(ctx)
	if !isExists {
		panic("client claims must be passed to ctx")
	}

	return claims
}

func (c *ClientClaims) ToCtx(parent context.Context) context.Context {
	return context.WithValue(parent, clientClaimsCtxKey{}, *c)
}
