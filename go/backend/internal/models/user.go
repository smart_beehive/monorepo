package models

import (
	"time"
)

type User struct {
	ID string `json:"id"`

	Email      string `json:"email"`
	FirstName  string `json:"first_name"`
	MiddleName string `json:"middle_name"`
	LastName   string `json:"last_name"`

	CreatedAt time.Time `json:"created_at"`
}
