package models

import (
	"time"

	"golang.org/x/oauth2"
)

type ExternalUser struct {
	InternalUID    string            `json:"internal_uid"`
	ExternalUID    string            `json:"external_uid"`
	Provider       OAuthProviderName `json:"provider"`
	Token          *oauth2.Token     `json:"token"`
	CreatedAt      time.Time         `json:"created_at"`
	UpdateTokenAt  *time.Time        `json:"update_token_at"`
	RefreshTokenAt *time.Time        `json:"refresh_token_at"`
}
