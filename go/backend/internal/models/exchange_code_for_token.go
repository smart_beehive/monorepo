package models

import (
	"time"
)

type ExchangeCodeForToken struct {
	ID           string       `json:"id"`
	UID          string       `json:"uid"`
	ClientClaims ClientClaims `json:"client_claims"`
	ExpiredAfter time.Time    `json:"expired_after"`
	CreatedAt    time.Time    `json:"created_at"`
}
