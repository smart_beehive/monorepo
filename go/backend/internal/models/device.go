package models

import "time"

type Device struct {
	ID           string       `json:"id"`
	UID          string       `json:"uid"`
	ClientClaims ClientClaims `json:"client_claims"`
	CreatedAt    time.Time    `json:"created_at"`
}
