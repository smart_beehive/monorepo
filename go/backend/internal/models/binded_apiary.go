package models

import "time"

type BindedApiary struct {
	ID        string     `json:"id"`
	UID       string     `json:"uid"`
	ApiaryID  string     `json:"apiary_id"`
	Name      string     `json:"name"`
	CreatedAt time.Time  `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at,omitempty"`
	DeletedAt *time.Time `json:"deleted_at,omitempty"`
}
