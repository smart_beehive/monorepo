package models

type OAuthProviderName string

const GoogleOAuthProvider OAuthProviderName = "google"
