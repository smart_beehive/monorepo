package repositories

import (
	"context"
	"time"

	"bitbucket.org/smart_beehive/backend/internal/models"
	"golang.org/x/oauth2"
)

type AuthTokens interface {
	Create(ctx context.Context, uid, did string, accessExpiredAfter, refreshExpiredAfter time.Time) (authToken *models.AuthToken, err error)
	ReadByRefreshToken(ctx context.Context, refreshToken string) (authToken *models.AuthToken, err error)
	ReadByAccessToken(ctx context.Context, accessToken string) (authToken *models.AuthToken, err error)
	Disable(ctx context.Context, accessToken string) error
}

type Devices interface {
	Create(ctx context.Context, uid string, clientClaims models.ClientClaims) (device *models.Device, err error)
	ReadByUIDAndClientClaims(ctx context.Context, uid string, clientClaims models.ClientClaims) (device *models.Device, err error)
}

type ExchangeCodeForTokens interface {
	Create(ctx context.Context, uid string, ClientClaims models.ClientClaims, expiredAfter time.Time) (exchangeCodeForToken *models.ExchangeCodeForToken, err error)
	ReadByIDAndClientClaims(ctx context.Context, code string, clientClaims models.ClientClaims) (exchangeCodeForToken *models.ExchangeCodeForToken, err error)
}

type Users interface {
	ReadByExternalUser(ctx context.Context, providerName models.OAuthProviderName, uid string) (user *models.User, err error)
	CreateByOAuthUser(ctx context.Context, oauthUser models.OAuthUser) (user *models.User, err error)
}

type ExternalUsers interface {
	ReadByExernalID(ctx context.Context, externalID string, provider models.OAuthProviderName) (user *models.ExternalUser, err error)
	Create(ctx context.Context, internalUID, externalUID string, provider models.OAuthProviderName, token *oauth2.Token) (user *models.ExternalUser, err error)
	UpdateToken(ctx context.Context, user *models.ExternalUser, token *oauth2.Token) (err error)
}

type OAuthConnectProviderSessions interface {
	Create(ctx context.Context, clientClaims models.ClientClaims, provider models.OAuthProviderName, expiredAfter time.Time) (session *models.OAuthConnectProviderSession, err error)
	ReadByIDAndClientClaims(ctx context.Context, state string, clientClaims models.ClientClaims) (session *models.OAuthConnectProviderSession, err error)
}

type Apiaries interface {
	Create(ctx context.Context) (apiary *models.Apiary, err error)
	ReadByID(ctx context.Context, id string) (apiary *models.Apiary, err error)
}

type BindedApiaries interface {
	CheckExists(ctx context.Context, apiaryID, uid string) (isBinded bool, err error)
	CheckOwner(ctx context.Context, id, uid string) (isOwner bool, err error)
	Create(ctx context.Context, apiaryID, uid, name string) (apiary *models.BindedApiary, err error)
	UpdateName(ctx context.Context, id, name string) (err error)
	Delete(ctx context.Context, id string) (err error)
	ReadByID(ctx context.Context, id string) (apiary *models.BindedApiary, err error)
	ReadPage(ctx context.Context, uid string, pageNumber int) (apiaries []*models.BindedApiary, err error)
	Count(ctx context.Context, uid string) (count int, err error)
}

type BeeHives interface {
	Create(ctx context.Context) (beehive *models.BeeHive, err error)
	ReadByID(ctx context.Context, id string) (beehive *models.BeeHive, err error)
}

type BindedBeeHives interface {
	CheckExists(ctx context.Context, beehiveID, uid string) (isBinded bool, err error)
	CheckOwner(ctx context.Context, id, uid string) (isOwner bool, err error)
	Create(ctx context.Context, bindApiaryID, beehiveID string) (beehive *models.BindedBeeHive, err error)
	ReadByID(ctx context.Context, id string) (beehive *models.BindedBeeHive, err error)
	RebindAll(ctx context.Context, bindApiaryID, newBindApiaryID string) (err error)
	Rebind(ctx context.Context, id, newBindApiaryID string) (beehive *models.BindedBeeHive, err error)
	DeleteAllByBindApiaryID(ctx context.Context, bindApiaryID string) (err error)
	Delete(ctx context.Context, id string) (err error)
	ReadPage(ctx context.Context, bindApiaryID string, pageNumber int) (beehives []*models.BindedBeeHive, err error)
	Count(ctx context.Context, bindApiaryID string) (count int, err error)
}
