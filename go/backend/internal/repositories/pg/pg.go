package pg

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
)

type DB interface {
	Exec(ctx context.Context, sql string, arguments ...interface{}) (commandTag pgconn.CommandTag, err error)
	Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error)
	QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row
	QueryFunc(ctx context.Context, sql string, args []interface{}, scans []interface{}, f func(pgx.QueryFuncRow) error) (pgconn.CommandTag, error)
}

type txCtxKey struct{}

func txFromCtx(ctx context.Context) (pgx.Tx, bool) {
	tx, ok := ctx.Value(txCtxKey{}).(pgx.Tx)

	return tx, ok
}

func txToCtx(parent context.Context, tx pgx.Tx) context.Context {
	return context.WithValue(parent, txCtxKey{}, tx)
}

type Transactor interface {
	Begin(ctx context.Context) (pgx.Tx, error)
}

func MakeTransactor(transactor Transactor, l logr.Logger) func(ctx context.Context, f func(txCtx context.Context) error) error {
	return func(ctx context.Context, f func(txCtx context.Context) error) error {
		tx, ok := txFromCtx(ctx)
		if ok {
			return f(ctx)
		}

		tx, err := transactor.Begin(ctx)
		if err != nil {
			return fmt.Errorf("transactor.Begin: %w", err)
		}

		txCtx := txToCtx(ctx, tx)

		if err = f(txCtx); err != nil {
			rollbackErr := tx.Rollback(ctx)
			if rollbackErr != nil {
				l.Error(rollbackErr, "tx.Rollback")
			}

			return fmt.Errorf("f: %w", err)
		}

		if err = tx.Commit(ctx); err != nil {
			return fmt.Errorf("tx.Commit: %w", err)
		}

		return nil
	}
}

func useTxFromContextOrDB(db DB, ctx context.Context) DB {
	tx, ok := txFromCtx(ctx)
	if ok {
		return tx
	}

	return db
}
