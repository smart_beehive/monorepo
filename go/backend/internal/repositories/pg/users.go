package pg

import (
	"context"
	"errors"
	"fmt"

	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/jackc/pgx/v4"
)

type Users struct {
	db DB
}

func NewUsers(db DB) *Users {
	return &Users{
		db: db,
	}
}

const querySelectUserByExternalUserID = `
SELECT id,
       email,
       first_name,
       middle_name,
       last_name,
       users.created_at
FROM users
         INNER JOIN external_users eu on users.id = eu.internal_uid
WHERE provider = $1
  AND external_uid = $2
LIMIT 1;
`

func (r *Users) ReadByExternalUser(ctx context.Context, provider models.OAuthProviderName, externalUID string) (user *models.User, err error) {
	user, err = r.scanRow(
		useTxFromContextOrDB(r.db, ctx).
			QueryRow(ctx, querySelectUserByExternalUserID, provider, externalUID),
	)
	if err != nil {
		return nil, fmt.Errorf("r.scanRow: %w", err)
	}

	return user, nil
}

const queryInsertUserWithOAuthUserData = `
INSERT INTO users (email, first_name, middle_name, last_name)
VALUES ($1, $2, $3, $4)
RETURNING id, created_at;
`

func (r *Users) CreateByOAuthUser(ctx context.Context, oauthUser models.OAuthUser) (user *models.User, err error) {
	user = &models.User{
		Email:      oauthUser.Email,
		FirstName:  oauthUser.FirstName,
		MiddleName: oauthUser.MiddleName,
		LastName:   oauthUser.LastName,
	}

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryInsertUserWithOAuthUserData, user.Email, user.FirstName, user.MiddleName, user.LastName).
		Scan(&user.ID, &user.CreatedAt)
	if err != nil {
		return nil, fmt.Errorf("useTxFromContextOrDB.QueryRow.Scan: %w", err)
	}

	return user, nil
}

func (r *Users) scanRows(rows pgx.Rows) (sessions []models.User, err error) {
	for rows.Next() {
		session, err := r.scanRow(rows)
		if err != nil {
			return nil, fmt.Errorf("r.scanRow: %w", err)
		}

		if session == nil {
			continue
		}

		sessions = append(sessions, *session)
	}

	return sessions, nil
}

func (r *Users) scanRow(row pgx.Row) (*models.User, error) {
	user := new(models.User)

	err := row.Scan(&user.ID, &user.Email, &user.FirstName, &user.MiddleName, &user.LastName, &user.CreatedAt)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	}

	if err != nil {
		return nil, fmt.Errorf("row.Scan: %w", err)
	}

	return user, nil
}
