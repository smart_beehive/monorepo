package pg

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/jackc/pgx/v4"
)

type Apiaries struct {
	db DB
}

func NewApiaries(db DB) *Apiaries {
	return &Apiaries{
		db: db,
	}
}

const queryInsertApiary = `
INSERT INTO apiaries (id)
VALUES (uuid_generate_v4())
RETURNING id, created_at;
`

func (r *Apiaries) Create(ctx context.Context) (apiary *models.Apiary, err error) {
	apiary = new(models.Apiary)

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryInsertApiary).
		Scan(&apiary.ID, &apiary.CreatedAt)
	if err != nil {
		return nil, fmt.Errorf("scan apiary: %w", err)
	}

	return apiary, nil
}

const querySelectApiaryByID = `
SELECT id, created_at, updated_at, deleted_at
FROM apiaries
WHERE id = $1
LIMIT 1;
`

func (r *Apiaries) ReadByID(ctx context.Context, id string) (apiary *models.Apiary, err error) {
	apiary, err = r.scanApiary(
		useTxFromContextOrDB(r.db, ctx).
			QueryRow(ctx, querySelectApiaryByID, id),
	)
	if err != nil {
		return nil, fmt.Errorf("scan apiary: %w", err)
	}

	return apiary, nil
}

func (r *Apiaries) scanApiary(row pgx.Row) (apiary *models.Apiary, err error) {
	apiary = new(models.Apiary)

	var (
		updatedAt sql.NullTime
		deletedAt sql.NullTime
	)

	err = row.Scan(&apiary.ID, &apiary.CreatedAt, &updatedAt, &deletedAt)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	}

	if err != nil {
		return nil, fmt.Errorf("row scan: %w", err)
	}

	if updatedAt.Valid {
		apiary.UpdatedAt = &updatedAt.Time
	}

	if deletedAt.Valid {
		apiary.DeletedAt = &deletedAt.Time
	}

	return apiary, nil
}
