package pg

import (
	"context"
	"errors"
	"fmt"
	"time"

	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/jackc/pgx/v4"
)

type OAuthConnectProviderSessions struct {
	db DB
}

func NewOAuthConnectProviderSessions(db DB) *OAuthConnectProviderSessions {
	return &OAuthConnectProviderSessions{
		db: db,
	}
}

const queryInsertOAuthConnectProviderSession = `
INSERT INTO oauth_connect_provider_sessions (client_claims, provider_name, expired_after)
VALUES ($1, $2, $3)
RETURNING id, started_at;
`

func (r *OAuthConnectProviderSessions) Create(ctx context.Context, clientClaims models.ClientClaims, provider models.OAuthProviderName, expiredAfter time.Time) (session *models.OAuthConnectProviderSession, err error) {
	session = &models.OAuthConnectProviderSession{
		ClientClaims: clientClaims,
		ProviderName: provider,
		ExpiredAfter: expiredAfter,
	}

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryInsertOAuthConnectProviderSession, clientClaims, provider, expiredAfter).
		Scan(&session.ID, &session.StartedAt)
	if err != nil {
		return nil, fmt.Errorf("useTxFromContextOrDB.QueryRow.Scan: %w", err)
	}

	return session, nil
}

const querySelectOAuthConnectProviderSessionByIDAndClientClaims = `
SELECT id, client_claims, provider_name, started_at, expired_after
FROM oauth_connect_provider_sessions
WHERE id = $1
  AND client_claims = $2
LIMIT 1;
`

func (r *OAuthConnectProviderSessions) ReadByIDAndClientClaims(ctx context.Context, state string, clientClaims models.ClientClaims) (session *models.OAuthConnectProviderSession, err error) {
	session, err = r.scanRow(
		useTxFromContextOrDB(r.db, ctx).
			QueryRow(ctx, querySelectOAuthConnectProviderSessionByIDAndClientClaims, state, clientClaims),
	)
	if err != nil {
		return nil, fmt.Errorf("r.scanRow: %w", err)
	}

	return session, nil
}

func (r *OAuthConnectProviderSessions) scanRows(rows pgx.Rows) (sessions []models.OAuthConnectProviderSession, err error) {
	for rows.Next() {
		session, err := r.scanRow(rows)
		if err != nil {
			return nil, fmt.Errorf("r.scanRow: %w", err)
		}

		if session == nil {
			continue
		}

		sessions = append(sessions, *session)
	}

	return sessions, nil
}

func (r *OAuthConnectProviderSessions) scanRow(row pgx.Row) (*models.OAuthConnectProviderSession, error) {
	session := new(models.OAuthConnectProviderSession)

	err := row.Scan(&session.ID, &session.ClientClaims, &session.ProviderName, &session.StartedAt, &session.ExpiredAfter)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	}

	if err != nil {
		return nil, fmt.Errorf("row.Scan: %w", err)
	}

	return session, nil
}
