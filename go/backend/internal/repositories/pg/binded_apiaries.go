package pg

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/jackc/pgx/v4"
)

type BindedApiaries struct {
	db    DB
	limit int
}

func NewBindedApiaries(db DB, limit int) *BindedApiaries {
	return &BindedApiaries{
		db:    db,
		limit: limit,
	}
}

const queryCheckExistsBindedApiaryByApiaryIDAndUID = `
SELECT 1
FROM binded_apiaries
WHERE apiary_id = $1
  AND uid = $2
  AND deleted_at IS NULL
LIMIT 1;
`

func (r *BindedApiaries) CheckExists(ctx context.Context, apiaryID, uid string) (isBinded bool, err error) {
	var isExists int

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryCheckExistsBindedApiaryByApiaryIDAndUID, apiaryID, uid).
		Scan(&isExists)
	if errors.Is(err, pgx.ErrNoRows) {
		return false, nil
	}

	if err != nil {
		return false, fmt.Errorf("row scan: %w", err)
	}

	return isExists == 1, nil
}

const queryCheckOwnerApiary = `
SELECT 1
FROM binded_apiaries
WHERE id = $1
  AND uid = $2
  AND deleted_at IS NULL
LIMIT 1;
`

func (r *BindedApiaries) CheckOwner(ctx context.Context, id, uid string) (isOwner bool, err error) {
	var count int

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryCheckOwnerApiary, id, uid).
		Scan(&count)
	if errors.Is(err, pgx.ErrNoRows) {
		return false, nil
	}

	if err != nil {
		return false, fmt.Errorf("check owner apiary: %w", err)
	}

	return count == 1, nil
}

const queryInsertBindedApiary = `
INSERT INTO binded_apiaries (uid, apiary_id, name)
VALUES ($1, $2, $3)
RETURNING id, created_at;
`

func (r *BindedApiaries) Create(ctx context.Context, apiaryID, uid, name string) (apiary *models.BindedApiary, err error) {
	apiary = new(models.BindedApiary)

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryInsertBindedApiary, apiaryID, uid, name).
		Scan(&apiary.ID, &apiary.CreatedAt)
	if err != nil {
		return nil, fmt.Errorf("scan row: %w", err)
	}

	return apiary, nil
}

const queryUpdateBindedApiaryName = `
UPDATE binded_apiaries
SET name = $2 AND updated_at = now()
WHERE id = $1;
`

func (r *BindedApiaries) UpdateName(ctx context.Context, id, name string) (err error) {
	_, err = useTxFromContextOrDB(r.db, ctx).Exec(ctx, queryUpdateBindedApiaryName, id, name)
	if err != nil {
		return fmt.Errorf("query exec: %w", err)
	}

	return nil
}

const queryDeleteBindedApiary = `
UPDATE binded_apiaries
SET deleted_at = now()
WHERE id = $1;
`

func (r *BindedApiaries) Delete(ctx context.Context, id string) (err error) {
	_, err = useTxFromContextOrDB(r.db, ctx).Exec(ctx, queryDeleteBindedApiary, id)
	if err != nil {
		return fmt.Errorf("query exec: %w", err)
	}

	return nil
}

const querySelectBindedApiaryByID = `
SELECT id, uid, apiary_id, name, created_at, updated_at, deleted_at
FROM binded_apiaries
WHERE id = $1 AND deleted_at IS NULL
LIMIT 1;
`

func (r *BindedApiaries) ReadByID(ctx context.Context, id string) (apiary *models.BindedApiary, err error) {
	apiary, err = r.scanBindedApiary(
		useTxFromContextOrDB(r.db, ctx).
			QueryRow(ctx, querySelectBindedApiaryByID, id),
	)
	if err != nil {
		return nil, fmt.Errorf("scan apiary: %w", err)
	}

	return apiary, nil
}

const querySelectBindedApiaryPage = `
SELECT id, uid, apiary_id, name, created_at, updated_at, deleted_at
FROM binded_apiaries
WHERE uid = $1 AND deleted_at IS NULL
OFFSET $2
LIMIT $3;
`

func (r *BindedApiaries) ReadPage(ctx context.Context, uid string, pageNumber int) (apiaries []*models.BindedApiary, err error) {
	rows, err := useTxFromContextOrDB(r.db, ctx).
		Query(ctx, querySelectBindedApiaryPage, uid, pageNumber*r.limit, r.limit)
	if err != nil {
		return nil, fmt.Errorf("select binded apiaries: %w", err)
	}

	apiaries, err = r.scanBindedApiaries(rows)
	if err != nil {
		return nil, fmt.Errorf("scan rows binded apiaries: %w", err)
	}

	return apiaries, nil
}

const queryCountBindedApiaries = `
SELECT count(1)
FROM binded_apiaries
WHERE uid = $1
  AND deleted_at IS NULL;
`

func (r *BindedApiaries) Count(ctx context.Context, uid string) (count int, err error) {
	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryCountBindedApiaries, uid).
		Scan(&count)
	if err != nil {
		return 0, fmt.Errorf("count binded apiaries: %w", err)
	}

	return count, nil
}

func (r *BindedApiaries) scanBindedApiaries(rows pgx.Rows) (apiaries []*models.BindedApiary, err error) {
	for rows.Next() {
		apiary, err := r.scanBindedApiary(rows)
		if err != nil {
			return nil, fmt.Errorf("scan binded apiary: %w", err)
		}

		apiaries = append(apiaries, apiary)
	}

	return apiaries, nil
}

func (r *BindedApiaries) scanBindedApiary(row pgx.Row) (apiary *models.BindedApiary, err error) {
	apiary = new(models.BindedApiary)

	var (
		updatedAt sql.NullTime
		deletedAt sql.NullTime
	)

	err = row.Scan(&apiary.ID, &apiary.UID, &apiary.ApiaryID, &apiary.Name, &apiary.CreatedAt, &updatedAt, &deletedAt)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	}

	if err != nil {
		return nil, fmt.Errorf("row scan: %w", err)
	}

	if updatedAt.Valid {
		apiary.UpdatedAt = &updatedAt.Time
	}

	if deletedAt.Valid {
		apiary.DeletedAt = &deletedAt.Time
	}

	return apiary, nil
}
