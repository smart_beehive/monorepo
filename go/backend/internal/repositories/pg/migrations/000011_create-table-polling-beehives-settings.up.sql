CREATE TABLE IF NOT EXISTS polling_beehives_settings
(
    bind_beehive_id     UUID UNIQUE              NOT NULL,
    polling_period_sec  int2                     NOT NULL,
    waiting_timeout_sec int2                     NOT NULL,
    audio_length_sec    int2                     NOT NULL,
    sample_rate         int2                     NOT NULL,
    created_at          timestamp with time zone NOT NULL DEFAULT now(),
    updated_At          timestamp with time zone NOT NULL,

    UNIQUE (bind_beehive_id),

    CONSTRAINT fk_bind_beehive
        FOREIGN KEY (bind_beehive_id)
            REFERENCES binded_beehives (id)
);