CREATE TABLE IF NOT EXISTS exchange_codes_for_token
(
    id            UUID PRIMARY KEY                  DEFAULT uuid_generate_v4(),
    uid           UUID                     NOT NULL,
    client_claims JSONB                    NOT NULL,
    expired_after timestamp with time zone NOT NULL,
    created_at    timestamp with time zone NOT NULL DEFAULT now(),

    CONSTRAINT fk_user
        FOREIGN KEY (uid)
            REFERENCES users (id)
);