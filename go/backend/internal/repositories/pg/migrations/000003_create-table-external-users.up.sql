CREATE TABLE IF NOT EXISTS external_users
(
    internal_uid     UUID                     NOT NULL,
    external_uid     TEXT                     NOT NULL,
    provider         oauth_provider           NOT NULL,
    token            JSONB,
    created_at       timestamp with time zone NOT NULL DEFAULT now(),
    update_token_at  timestamp with time zone,
    refresh_token_at timestamp with time zone,

    UNIQUE (internal_uid, provider),
    UNIQUE (provider, external_uid),

    CONSTRAINT fk_internal_user
        FOREIGN KEY (internal_uid)
            REFERENCES users (id)
);