BEGIN TRANSACTION;

CREATE TABLE IF NOT EXISTS event_types
(
    id   INTEGER PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS event_type_values
(
    id            INTEGER PRIMARY KEY,
    event_type_id INTEGER NOT NULL,
    name          TEXT    NOT NULL,

    CONSTRAINT fk_event_type
        FOREIGN KEY (event_type_id)
            REFERENCES event_types (id)
);

CREATE TABLE IF NOT EXISTS events
(
    id                  BIGSERIAL PRIMARY KEY,
    bind_apiary_id      UUID                     NOT NULL,
    bind_beehive_id     UUID                     NOT NULL,
    event_type_id       INTEGER                  NOT NULL,
    event_type_value_id INTEGER                  NOT NULL,
    is_watched          BOOLEAN                  NOT NULL DEFAULT FALSE,
    created_at          timestamp with time zone NOT NULL DEFAULT now(),
    watched_At          timestamp with time zone          DEFAULT NULL,

    CONSTRAINT fk_bind_apiary
        FOREIGN KEY (bind_apiary_id)
            REFERENCES binded_apiaries (id),
    CONSTRAINT fk_bind_beehive
        FOREIGN KEY (bind_beehive_id)
            REFERENCES binded_beehives (id),
    CONSTRAINT fk_event_type
        FOREIGN KEY (event_type_id)
            REFERENCES event_types (id),
    CONSTRAINT fk_event_type_value
        FOREIGN KEY (event_type_value_id)
            REFERENCES event_type_values (id)
);

COMMIT TRANSACTION;