BEGIN TRANSACTION;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public CASCADE;

DO
$$
    BEGIN
        IF NOT EXISTS(
                SELECT 1
                FROM pg_type
                WHERE typname = 'oauth_provider'
            ) THEN CREATE TYPE oauth_provider AS ENUM ('google');
        END IF;
    END
$$;

CREATE TABLE IF NOT EXISTS oauth_connect_provider_sessions
(
    id            UUID PRIMARY KEY                  DEFAULT uuid_generate_v4(),
    client_claims JSONB                    NOT NULL,
    provider_name oauth_provider           NOT NULL,
    started_at    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    expired_after TIMESTAMP WITH TIME ZONE NOT NULL
);

COMMIT TRANSACTION;