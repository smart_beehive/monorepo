BEGIN TRANSACTION;

DROP TABLE oauth_connect_provider_sessions;

DO
$$
    BEGIN
        IF EXISTS(SELECT 1 FROM pg_type WHERE typname = 'oauth_provider') THEN
            DROP TYPE oauth_provider;
        END IF;
    END
$$;

DROP EXTENSION IF EXISTS "uuid-ossp";

COMMIT TRANSACTION;