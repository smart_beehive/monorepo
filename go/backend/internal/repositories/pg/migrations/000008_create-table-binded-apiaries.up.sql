CREATE TABLE IF NOT EXISTS binded_apiaries
(
    id         UUID PRIMARY KEY                  DEFAULT uuid_generate_v4(),
    uid        UUID                     NOT NULL,
    apiary_id  UUID                     NOT NULL,
    name       varchar(255)             NOT NULL,
    created_at timestamp with time zone NOT NULL DEFAULT now(),
    updated_at timestamp with time zone          DEFAULT NULL,
    deleted_at timestamp with time zone          DEFAULT NULL,

    CONSTRAINT fk_user
        FOREIGN KEY (uid)
            REFERENCES users (id),

    CONSTRAINT fk_apiary
        FOREIGN KEY (apiary_id)
            REFERENCES apiaries (id),

    UNIQUE (uid, apiary_id)
);