CREATE TABLE IF NOT EXISTS beehives
(
    id         UUID PRIMARY KEY                  DEFAULT uuid_generate_v4(),
    created_at timestamp with time zone NOT NULL DEFAULT now(),
    updated_at timestamp with time zone          DEFAULT NULL,
    deleted_at timestamp with time zone          DEFAULT NULL
);