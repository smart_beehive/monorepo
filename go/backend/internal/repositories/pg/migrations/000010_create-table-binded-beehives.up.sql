CREATE TABLE IF NOT EXISTS binded_beehives
(
    id             UUID PRIMARY KEY                  DEFAULT uuid_generate_v4(),
    bind_apiary_id UUID                     NOT NULL,
    beehive_id     UUID                     NOT NULL,
    created_at     timestamp with time zone NOT NULL DEFAULT now(),
    updated_at     timestamp with time zone          DEFAULT NULL,
    deleted_at     timestamp with time zone          DEFAULT NULL,

    CONSTRAINT fk_bind_apiary
        FOREIGN KEY (bind_apiary_id)
            REFERENCES binded_apiaries (id),

    CONSTRAINT fk_beehive
        FOREIGN KEY (beehive_id)
            REFERENCES beehives (id),

    UNIQUE (bind_apiary_id, beehive_id)
);