CREATE TABLE IF NOT EXISTS users
(
    id           UUID PRIMARY KEY                  DEFAULT uuid_generate_v4(),
    email        TEXT                     NOT NULL,
    first_name   TEXT                     NOT NULL,
    middle_name  TEXT,
    last_name    TEXT                     NOT NULL,
    created_at   timestamp with time zone NOT NULL DEFAULT now()
);