BEGIN TRANSACTION;

CREATE TABLE IF NOT EXISTS telemetry
(
    bind_beehive_id UUID,
    ts              timestamp with time zone NOT NULL,
    temp            integer[]                NOT NULL,
    humidity        integer[]                NOT NULL,
    pressure        integer[]                NOT NULL,
    gpc             text                     NOT NULL,

    CONSTRAINT fk_bind_beehive
        FOREIGN KEY (bind_beehive_id)
            REFERENCES binded_beehives (id)
);

CREATE TABLE IF NOT EXISTS audio
(
    bind_beehive_id  UUID,
    ts               timestamp with time zone NOT NULL,
    body             bytea                    NOT NULL,
    audio_length_sec int2                     NOT NULL,
    sample_rate      int2                     NOT NULL,

    CONSTRAINT fk_bind_beehive
        FOREIGN KEY (bind_beehive_id)
            REFERENCES binded_beehives (id)
);

COMMIT TRANSACTION;