CREATE TABLE IF NOT EXISTS auth_tokens
(
    uid                   UUID                     NOT NULL,
    did                   UUID                     NOT NULL,
    access_token          UUID UNIQUE              NOT NULL DEFAULT uuid_generate_v4(),
    refresh_token         UUID UNIQUE              NOT NULL DEFAULT uuid_generate_v4(),
    access_expired_after  timestamp with time zone NOT NULL,
    refresh_expired_after timestamp with time zone NOT NULL,
    is_enabled            boolean                  NOT NULL DEFAULT true,
    created_at            timestamp with time zone NOT NULL DEFAULT now(),
    disabled_at           timestamp with time zone,

    CONSTRAINT fk_user
        FOREIGN KEY (uid)
            REFERENCES users (id),
    CONSTRAINT fk_device
        FOREIGN KEY (did)
            REFERENCES devices (id)
);