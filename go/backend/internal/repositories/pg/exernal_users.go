package pg

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/jackc/pgx/v4"
	"golang.org/x/oauth2"
)

type ExternalUsers struct {
	db DB
}

func NewExternalUsers(db DB) *ExternalUsers {
	return &ExternalUsers{
		db: db,
	}
}

const querySelectExternalUsersByID = `
SELECT internal_uid, external_uid, provider, token, created_at, update_token_at, refresh_token_at
FROM external_users
WHERE external_uid = $1
  AND provider = $2
LIMIT 1;
`

func (r *ExternalUsers) ReadByExernalID(ctx context.Context, externalID string, provider models.OAuthProviderName) (user *models.ExternalUser, err error) {
	user, err = r.scanRow(
		useTxFromContextOrDB(r.db, ctx).
			QueryRow(ctx, querySelectExternalUsersByID, externalID, provider),
	)
	if err != nil {
		return nil, fmt.Errorf("r.scanRow: %w", err)
	}

	return user, nil
}

const queryInsertExternalUser = `
INSERT INTO external_users (internal_uid, external_uid, provider, token)
VALUES ($1, $2, $3, $4)
RETURNING created_at;
`

func (r *ExternalUsers) Create(ctx context.Context, internalUID string, externalUID string, provider models.OAuthProviderName, token *oauth2.Token) (user *models.ExternalUser, err error) {
	user = &models.ExternalUser{
		InternalUID: internalUID,
		ExternalUID: externalUID,
		Provider:    provider,
		Token:       token,
	}

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryInsertExternalUser, user.InternalUID, user.ExternalUID, user.Provider, user.Token).
		Scan(&user.CreatedAt)
	if err != nil {
		return nil, fmt.Errorf("useTxFromContextOrDB.QueryRow.Scan: %w", err)
	}

	return user, nil
}

const queryUpdateExternalUserSetToken = `
UPDATE external_users
SET token           = $4,
    update_token_at = now()
WHERE internal_uid = $1
  AND external_uid = $2
  AND provider = $3
RETURNING update_token_at;
`

func (r *ExternalUsers) UpdateToken(ctx context.Context, user *models.ExternalUser, token *oauth2.Token) (err error) {
	if user == nil {
		return nil
	}

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryUpdateExternalUserSetToken, user.InternalUID, user.ExternalUID, user.Provider, token).
		Scan(&user.UpdateTokenAt)
	if err != nil {
		return fmt.Errorf("useTxFromContextOrDB.QueryRow.Scan: %w", err)
	}

	return nil
}

func (r *ExternalUsers) scanRows(rows pgx.Rows) (sessions []models.ExternalUser, err error) {
	for rows.Next() {
		session, err := r.scanRow(rows)
		if err != nil {
			return nil, fmt.Errorf("r.scanRow: %w", err)
		}

		if session == nil {
			continue
		}

		sessions = append(sessions, *session)
	}

	return sessions, nil
}

func (r *ExternalUsers) scanRow(row pgx.Row) (*models.ExternalUser, error) {
	user := new(models.ExternalUser)

	var (
		updateTokenAt, refreshTokenAt sql.NullTime
	)

	err := row.Scan(
		&user.InternalUID, &user.ExternalUID, &user.Provider, &user.Token, &user.CreatedAt,
		&updateTokenAt, &refreshTokenAt,
	)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	}

	if err != nil {
		return nil, fmt.Errorf("row.Scan: %w", err)
	}

	if updateTokenAt.Valid {
		user.UpdateTokenAt = &updateTokenAt.Time
	}

	if refreshTokenAt.Valid {
		user.RefreshTokenAt = &refreshTokenAt.Time
	}

	return user, nil
}
