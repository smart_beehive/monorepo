package pg

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/jackc/pgx/v4"
)

type AuthTokens struct {
	db DB
}

func NewAuthTokens(db DB) *AuthTokens {
	return &AuthTokens{
		db: db,
	}
}

const queryInsertAuthToken = `
INSERT INTO auth_tokens (uid, did, access_expired_after, refresh_expired_after)
VALUES ($1, $2, $3, $4)
RETURNING access_token, refresh_token, is_enabled, created_at;
`

func (r *AuthTokens) Create(ctx context.Context, uid, did string, accessExpiredAfter, refreshExpiredAfter time.Time) (authToken *models.AuthToken, err error) {
	authToken = &models.AuthToken{
		UID:                 uid,
		DID:                 did,
		AccessExpiredAfter:  accessExpiredAfter,
		RefreshExpiredAfter: refreshExpiredAfter,
	}

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryInsertAuthToken, uid, did, accessExpiredAfter, refreshExpiredAfter).
		Scan(&authToken.AccessToken, &authToken.RefreshToken, &authToken.IsEnabled, &authToken.CreatedAt)
	if err != nil {
		return nil, fmt.Errorf("useTxFromContextOrDB.QueryRow.Scan: %w", err)
	}

	return authToken, nil
}

const querySelectAuthTokenByRefreshToken = `
SELECT uid,
       did,
       access_token,
       refresh_token,
       access_expired_after,
       refresh_expired_after,
       is_enabled,
       created_at,
       disabled_at
FROM auth_tokens
WHERE refresh_token = $1
LIMIT 1;
`

func (r *AuthTokens) ReadByRefreshToken(ctx context.Context, refreshToken string) (authToken *models.AuthToken, err error) {
	authToken, err = r.scanRow(
		useTxFromContextOrDB(r.db, ctx).
			QueryRow(ctx, querySelectAuthTokenByRefreshToken, refreshToken),
	)
	if err != nil {
		return nil, fmt.Errorf("r.scanRow: %w", err)
	}

	return authToken, nil
}

const querySelectAuthTokenByAccessToken = `
SELECT uid,
       did,
       access_token,
       refresh_token,
       access_expired_after,
       refresh_expired_after,
       is_enabled,
       created_at,
       disabled_at
FROM auth_tokens
WHERE access_token = $1
LIMIT 1;
`

func (r *AuthTokens) ReadByAccessToken(ctx context.Context, accessToken string) (authToken *models.AuthToken, err error) {
	authToken, err = r.scanRow(
		useTxFromContextOrDB(r.db, ctx).
			QueryRow(ctx, querySelectAuthTokenByAccessToken, accessToken),
	)
	if err != nil {
		return nil, fmt.Errorf("r.scanRow: %w", err)
	}

	return authToken, nil
}

const queryUpdateAuthTokenSetIsEnabledFalse = `
UPDATE auth_tokens
SET is_enabled  = false,
    disabled_at = now()
WHERE access_token = $1;
`

func (r *AuthTokens) Disable(ctx context.Context, accessToken string) error {
	_, err := useTxFromContextOrDB(r.db, ctx).
		Exec(ctx, queryUpdateAuthTokenSetIsEnabledFalse, accessToken)
	if err != nil {
		return fmt.Errorf("useTxFromContextOrDB.Exec: %w", err)
	}

	return nil
}

func (r *AuthTokens) scanRows(rows pgx.Rows) (authTokens []models.AuthToken, err error) {
	for rows.Next() {
		authToken, err := r.scanRow(rows)
		if err != nil {
			return nil, fmt.Errorf("r.scanRow: %w", err)
		}

		if authToken == nil {
			continue
		}

		authTokens = append(authTokens, *authToken)
	}

	return authTokens, nil
}

func (r *AuthTokens) scanRow(row pgx.Row) (authToken *models.AuthToken, err error) {
	authToken = new(models.AuthToken)

	var disabledAt sql.NullTime

	err = row.Scan(&authToken.UID, &authToken.DID, &authToken.AccessToken, &authToken.RefreshToken,
		&authToken.AccessExpiredAfter, &authToken.RefreshExpiredAfter, &authToken.IsEnabled,
		&authToken.CreatedAt, &disabledAt)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	}

	if err != nil {
		return nil, fmt.Errorf("row.Scan: %w", err)
	}

	if disabledAt.Valid {
		authToken.DisabledAt = &disabledAt.Time
	}

	return authToken, nil
}
