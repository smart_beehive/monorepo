package pg

import (
	"context"
	"errors"
	"fmt"
	"time"

	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/jackc/pgx/v4"
)

type ExchangeCodeForTokens struct {
	db DB
}

func NewExchangeCodeForTokens(db DB) *ExchangeCodeForTokens {
	return &ExchangeCodeForTokens{
		db: db,
	}
}

const queryInsertExchangeCodeForToken = `
INSERT INTO exchange_codes_for_token (uid, client_claims, expired_after)
VALUES ($1, $2, $3)
RETURNING id, created_at
`

func (r *ExchangeCodeForTokens) Create(ctx context.Context, uid string, clientClaims models.ClientClaims, expiredAfter time.Time) (exchangeCodeForToken *models.ExchangeCodeForToken, err error) {
	exchangeCodeForToken = &models.ExchangeCodeForToken{
		UID:          uid,
		ClientClaims: clientClaims,
		ExpiredAfter: expiredAfter,
	}

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryInsertExchangeCodeForToken, uid, clientClaims, expiredAfter).
		Scan(&exchangeCodeForToken.ID, &exchangeCodeForToken.CreatedAt)
	if err != nil {
		return nil, fmt.Errorf("useTxFromContextOrDB.QueryRow.Scan: %w", err)
	}

	return exchangeCodeForToken, nil
}

const querySelectExchangeCodeForTokenByIDAndClientClaims = `
SELECT id, uid, client_claims, expired_after, created_at
FROM exchange_codes_for_token
WHERE id = $1
  AND client_claims = $2
LIMIT 1;
`

func (r *ExchangeCodeForTokens) ReadByIDAndClientClaims(ctx context.Context, code string, clientClaims models.ClientClaims) (exchangeCodeForToken *models.ExchangeCodeForToken, err error) {
	exchangeCodeForToken, err = r.scanRow(
		useTxFromContextOrDB(r.db, ctx).
			QueryRow(ctx, querySelectExchangeCodeForTokenByIDAndClientClaims, code, clientClaims),
	)
	if err != nil {
		return nil, fmt.Errorf("r.scanRow: %w", err)
	}

	return exchangeCodeForToken, nil
}

func (r *ExchangeCodeForTokens) scanRow(row pgx.Row) (exchangeCodeForToken *models.ExchangeCodeForToken, err error) {
	exchangeCodeForToken = new(models.ExchangeCodeForToken)

	err = row.Scan(&exchangeCodeForToken.ID, &exchangeCodeForToken.UID, &exchangeCodeForToken.ClientClaims,
		&exchangeCodeForToken.ExpiredAfter, &exchangeCodeForToken.CreatedAt)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	}

	if err != nil {
		return nil, fmt.Errorf("row.Scan: %w", err)
	}

	return exchangeCodeForToken, nil
}
