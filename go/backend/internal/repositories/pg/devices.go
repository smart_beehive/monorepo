package pg

import (
	"context"
	"errors"
	"fmt"

	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/jackc/pgx/v4"
)

type Devices struct {
	db DB
}

func NewDevices(db DB) *Devices {
	return &Devices{
		db: db,
	}
}

const queryInsertDevice = `
INSERT INTO devices (uid, client_claims)
VALUES ($1, $2)
RETURNING id, created_at;
`

func (r *Devices) Create(ctx context.Context, uid string, clientClaims models.ClientClaims) (device *models.Device, err error) {
	device = &models.Device{
		UID:          uid,
		ClientClaims: clientClaims,
	}

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryInsertDevice, uid, clientClaims).
		Scan(&device.ID, &device.CreatedAt)
	if err != nil {
		return nil, fmt.Errorf("useTxFromContextOrDB.QueryRow.Scan: %w", err)
	}

	return device, nil
}

const querySelectDeviceByUIDAndClientClaims = `
SELECT id, uid, client_claims, created_at
FROM devices
WHERE uid = $1
  AND client_claims = $2
LIMIT 1;
`

func (r *Devices) ReadByUIDAndClientClaims(ctx context.Context, uid string, clientClaims models.ClientClaims) (device *models.Device, err error) {
	device, err = r.scanRow(
		useTxFromContextOrDB(r.db, ctx).
			QueryRow(ctx, querySelectDeviceByUIDAndClientClaims, uid, clientClaims),
	)
	if err != nil {
		return nil, fmt.Errorf("r.scanRow: %w", err)
	}

	return device, nil
}

func (r *Devices) scanRows(rows pgx.Rows) (devices []models.Device, err error) {
	for rows.Next() {
		device, err := r.scanRow(rows)
		if err != nil {
			return nil, fmt.Errorf("r.scanRow: %w", err)
		}

		if device == nil {
			continue
		}

		devices = append(devices, *device)
	}

	return devices, nil
}

func (r *Devices) scanRow(row pgx.Row) (device *models.Device, err error) {
	device = new(models.Device)

	err = row.Scan(&device.ID, &device.UID, &device.ClientClaims, &device.CreatedAt)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	}

	if err != nil {
		return nil, fmt.Errorf("row.Scan: %w", err)
	}

	return device, nil
}
