package pg

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/jackc/pgx/v4"
)

type BindedBeeHives struct {
	db    DB
	limit int
}

func NewBindedBeeHives(db DB, limit int) *BindedBeeHives {
	return &BindedBeeHives{
		db:    db,
		limit: limit,
	}
}

const queryCheckExistsBindedBeehive = `
SELECT 1
FROM binded_beehives as bbs
         INNER JOIN binded_apiaries as bas ON bas.id = bbs.bind_apiary_id AND bas.uid = $2
WHERE bbs.beehive_id = $1
  AND bbs.deleted_at IS NULL
LIMIT 1;
`

func (r *BindedBeeHives) CheckExists(ctx context.Context, beehiveID string, uid string) (isBinded bool, err error) {
	var count int

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryCheckExistsBindedBeehive, beehiveID, uid).
		Scan(&count)
	if errors.Is(err, pgx.ErrNoRows) {
		return false, nil
	}

	if err != nil {
		return false, fmt.Errorf("query row check exists binded beehive: %w", err)
	}

	return count == 1, nil
}

const queryCheckOwnerBindedBeehive = `
SELECT 1
FROM binded_beehives as bbs
         INNER JOIN binded_apiaries as bas ON bas.id = bbs.bind_apiary_id AND bas.uid = $2
WHERE bbs.id = $1
  AND bbs.deleted_at IS NULL
LIMIT 1;
`

func (r *BindedBeeHives) CheckOwner(ctx context.Context, id string, uid string) (isOwner bool, err error) {
	var count int

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryCheckOwnerBindedBeehive, id, uid).
		Scan(&count)
	if errors.Is(err, pgx.ErrNoRows) {
		return false, nil
	}

	if err != nil {
		return false, fmt.Errorf("query row check owner binded beehive: %w", err)
	}

	return count == 1, nil
}

const queryInsertBindedBeehive = `
INSERT INTO binded_beehives (bind_apiary_id, beehive_id)
VALUES ($1, $2)
RETURNING id, created_at;
`

func (r *BindedBeeHives) Create(ctx context.Context, bindApiaryID string, beehiveID string) (beehive *models.BindedBeeHive, err error) {
	beehive = new(models.BindedBeeHive)
	beehive.BindApiaryID = bindApiaryID
	beehive.BeeHiveID = beehiveID

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryInsertBindedBeehive, bindApiaryID, beehiveID).
		Scan(&beehive.ID, &beehive.CreatedAt)
	if err != nil {
		return nil, fmt.Errorf("query insert row binded beehive: %w", err)
	}

	return beehive, nil
}

const querySelectBindedBeehiveByID = `
SELECT id, bind_apiary_id, beehive_id, created_at, updated_at, deleted_at
FROM binded_beehives
WHERE id = $1
  AND deleted_at IS NULL
LIMIT 1;
`

func (r *BindedBeeHives) ReadByID(ctx context.Context, id string) (beehive *models.BindedBeeHive, err error) {
	beehive, err = r.scanBindedBeehive(
		useTxFromContextOrDB(r.db, ctx).QueryRow(ctx, querySelectBindedBeehiveByID, id),
	)
	if err != nil {
		return nil, fmt.Errorf("select binded beehive by id: %w", err)
	}

	return beehive, nil
}

const queryDeleteBindedBeehive = `
UPDATE binded_beehives
SET deleted_at = now()
WHERE id = $1;
`

func (r *BindedBeeHives) Delete(ctx context.Context, id string) (err error) {
	_, err = useTxFromContextOrDB(r.db, ctx).
		Exec(ctx, queryDeleteBindedBeehive, id)
	if err != nil {
		return fmt.Errorf("query update deleted_at field in binded beehive: %w", err)
	}

	return nil
}

const querySelectBindedBeehivesPage = `
SELECT id, bind_apiary_id, beehive_id, created_at, updated_at, deleted_at
FROM binded_beehives
WHERE bind_apiary_id = $1 AND deleted_at IS NULL
OFFSET $2
LIMIT $3;
`

func (r *BindedBeeHives) ReadPage(ctx context.Context, bindApiaryID string, pageNumber int) (beehives []*models.BindedBeeHive, err error) {
	rows, err := useTxFromContextOrDB(r.db, ctx).
		Query(ctx, querySelectBindedBeehivesPage, bindApiaryID, pageNumber*r.limit, r.limit)
	if err != nil {
		return nil, fmt.Errorf("select binded beehives: %w", err)
	}

	beehives, err = r.scanBindedBeehives(rows)
	if err != nil {
		return nil, fmt.Errorf("scan rows binded beehives: %w", err)
	}

	return beehives, nil
}

const queryCountBindedBeeHives = `
SELECT count(1)
FROM binded_beehives
WHERE bind_apiary_id = $1
  AND deleted_at IS NULL;
`

func (r *BindedBeeHives) Count(ctx context.Context, bindApiaryID string) (count int, err error) {
	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryCountBindedBeeHives, bindApiaryID).
		Scan(&count)
	if err != nil {
		return 0, fmt.Errorf("count binded beehives: %w", err)
	}

	return count, nil
}

const queryInsertUnbindedBeeHive = `
WITH updated_rows AS (
	UPDATE binded_beehives
	    SET deleted_at = now()
	    WHERE id = $1
	    RETURNING beehive_id
  )
  INSERT
  INTO binded_beehives (bind_apiary_id, beehive_id)
  SELECT $2, beehive_id
  FROM updated_rows
  RETURNING id, beehive_id, created_at;
`

func (r *BindedBeeHives) Rebind(ctx context.Context, id string, newBindApiaryID string) (beehive *models.BindedBeeHive, err error) {
	beehive = new(models.BindedBeeHive)
	beehive.BindApiaryID = newBindApiaryID

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryInsertUnbindedBeeHive, id, newBindApiaryID).
		Scan(&beehive.ID, &beehive.BeeHiveID, &beehive.CreatedAt)
	if err != nil {
		return nil, fmt.Errorf("query insert row binded beehive: %w", err)
	}

	return beehive, nil
}

const queryInsertUnbindedBeeHives = `
WITH updated_rows AS (
	UPDATE binded_beehives
	    SET deleted_at = now()
	    WHERE bind_apiary_id = $1
	    RETURNING beehive_id
  )
  INSERT
  INTO binded_beehives (bind_apiary_id, beehive_id)
  SELECT $2, beehive_id
  FROM updated_rows;
`

func (r *BindedBeeHives) RebindAll(ctx context.Context, bindApiaryID, newBindApiaryID string) (err error) {
	_, err = useTxFromContextOrDB(r.db, ctx).
		Exec(ctx, queryInsertUnbindedBeeHives, bindApiaryID, newBindApiaryID)
	if err != nil {
		return fmt.Errorf("update and insert unbinded beehives: %w", err)
	}

	return nil
}

const queryDeleteBindedBeehivesByBindApiaryID = `
UPDATE binded_beehives
SET deleted_at = now()
WHERE bind_apiary_id = $1;
`

func (r *BindedBeeHives) DeleteAllByBindApiaryID(ctx context.Context, bindApiaryID string) (err error) {
	_, err = useTxFromContextOrDB(r.db, ctx).
		Exec(ctx, queryDeleteBindedBeehivesByBindApiaryID, bindApiaryID)
	if err != nil {
		return fmt.Errorf("query update unbind beehives: %w", err)
	}

	return nil
}

func (r *BindedBeeHives) scanBindedBeehives(rows pgx.Rows) (beehives []*models.BindedBeeHive, err error) {
	for rows.Next() {
		beehive, err := r.scanBindedBeehive(rows)
		if err != nil {
			return nil, fmt.Errorf("scan binded beehive: %w", err)
		}

		beehives = append(beehives, beehive)
	}

	return beehives, nil
}

func (r *BindedBeeHives) scanBindedBeehive(row pgx.Row) (beehive *models.BindedBeeHive, err error) {
	beehive = new(models.BindedBeeHive)

	var (
		updatedAt sql.NullTime
		deletedAt sql.NullTime
	)

	// id, bind_apiary_id, beehive_id, created_at, updated_at, deleted_at
	err = row.Scan(&beehive.ID, &beehive.BindApiaryID, &beehive.BeeHiveID, &beehive.CreatedAt, &updatedAt, &deletedAt)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	}

	if err != nil {
		return nil, fmt.Errorf("row scan: %w", err)
	}

	if updatedAt.Valid {
		beehive.UpdatedAt = &updatedAt.Time
	}

	if deletedAt.Valid {
		beehive.DeletedAt = &deletedAt.Time
	}

	return beehive, nil
}
