package pg

import (
	"context"
	"fmt"

	"bitbucket.org/smart_beehive/backend/internal/models"
)

type BeeHives struct {
	db DB
}

func NewBeeHives(db DB) *BeeHives {
	return &BeeHives{db: db}
}

const queryInsertBeeHive = `
INSERT INTO beehives (id)
VALUES (uuid_generate_v4())
RETURNING id, created_at;
`

func (r *BeeHives) Create(ctx context.Context) (beehive *models.BeeHive, err error) {
	beehive = new(models.BeeHive)

	err = useTxFromContextOrDB(r.db, ctx).
		QueryRow(ctx, queryInsertBeeHive).
		Scan(&beehive.ID, &beehive.CreatedAt)
	if err != nil {
		return nil, fmt.Errorf("scan apiary: %w", err)
	}

	return beehive, nil
}

func (r *BeeHives) ReadByID(ctx context.Context, id string) (beehive *models.BeeHive, err error) {
	panic("not implemented") // TODO: Implement
}
