package proc

import (
	"context"

	"golang.org/x/sync/errgroup"
)

func ParallelJobs(ctx context.Context, jobs ...func(ctx context.Context) error) error {
	group, ctx := errgroup.WithContext(ctx)

	for _, job := range jobs {
		internalJob := job

		group.Go(func() error {
			return internalJob(ctx)
		})
	}

	return group.Wait()
}
