package log

import (
	"fmt"

	"github.com/go-logr/logr"
	"github.com/go-logr/zapr"
	"go.uber.org/zap"
)

func MakeLogger() logr.Logger {
	config := zap.NewDevelopmentConfig()
	config.DisableStacktrace = true

	zapLog, err := config.Build()
	if err != nil {
		panic(fmt.Sprintf("who watches the watchmen (%v)?", err))
	}

	return zapr.NewLogger(zapLog)
}
