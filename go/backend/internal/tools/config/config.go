package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

const configFileName = ".env"

func LoadConfig(config interface{}) (err error) {
	if _, err = os.Stat(configFileName); !os.IsNotExist(err) {
		if err = godotenv.Load(configFileName); err != nil {
			return fmt.Errorf("godotenv.Load: %w", err)
		}
	}

	err = envconfig.Process("", config)
	if err != nil {
		return fmt.Errorf("envconfig.Process: %w", err)
	}

	return nil
}

func WriteConfigInfoToStdout(config interface{}) (err error) {
	err = envconfig.Usagef("", config, os.Stdout, envconfig.DefaultListFormat)
	if err != nil {
		return err
	}

	return nil
}

const configEnvFileFormat = `{{range .}}# Описание: {{usage_description .}}
# Тип: {{usage_type .}}
{{if eq (usage_required .) "true"}}# Обязательное использование.{{else}}# Не обязательное использование (для использования раскоментировать и указать значение).{{end}}
#{{usage_key .}}={{usage_default .}}

{{end}}`

func InitConfigFile(config interface{}) (err error) {
	if _, err = os.Stat(configFileName); !os.IsNotExist(err) {
		err = os.Rename(configFileName, fmt.Sprintf("%s.%s", configFileName, "backup"))
		if err != nil {
			return fmt.Errorf("godotenv.Load: %w", err)
		}
	}

	f, err := os.Create(configFileName)
	if err != nil {
		return err
	}
	defer f.Close()

	err = envconfig.Usagef("", config, f, configEnvFileFormat)
	if err != nil {
		return err
	}

	return nil
}
