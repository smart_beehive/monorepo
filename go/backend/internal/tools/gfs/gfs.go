package gfs

import (
	"context"
	"errors"
	"os"
	"os/signal"
	"syscall"
)

type ErrOSSignal struct {
	signal os.Signal
}

func (e *ErrOSSignal) Error() string {
	return e.signal.String()
}

func IsErrOSSignal(err error) bool {
	var errOSSignal *ErrOSSignal

	return errors.As(err, &errOSSignal)
}

func SignalNotify(ctx context.Context) error {
	c := make(chan os.Signal, 1)

	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)

	select {
	case s := <-c:
		return &ErrOSSignal{signal: s}
	case <-ctx.Done():
		return ctx.Err()
	}
}
