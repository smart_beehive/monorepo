package tx

import (
	"context"

	"bitbucket.org/smart_beehive/backend/internal"
	"bitbucket.org/smart_beehive/backend/internal/models"
)

type AuthService struct {
	transactor Transactor
	internal.AuthService
}

func MakeForAuthService(transactor Transactor) internal.AuthServiceMW {
	return func(next internal.AuthService) internal.AuthService {
		return &AuthService{
			transactor:  transactor,
			AuthService: next,
		}
	}
}

func (s *AuthService) ConnectProvider(ctx context.Context, providerName models.OAuthProviderName) (redirectURLToProvider string, err error) {
	err = s.transactor(ctx, func(ctxWithTx context.Context) error {
		redirectURLToProvider, err = s.AuthService.ConnectProvider(ctx, providerName)

		return err
	})

	return redirectURLToProvider, err
}

func (s *AuthService) CallbackFromProvider(ctx context.Context, code string, state string) (redirectURLToToken string, err error) {
	err = s.transactor(ctx, func(ctxWithTx context.Context) error {
		redirectURLToToken, err = s.AuthService.CallbackFromProvider(ctx, code, state)

		return err
	})

	return redirectURLToToken, err
}

func (s *AuthService) Token(ctx context.Context, internalCode string) (token *models.AuthToken, err error) {
	err = s.transactor(ctx, func(ctxWithTx context.Context) error {
		token, err = s.AuthService.Token(ctx, internalCode)

		return err
	})

	return token, err
}

func (s *AuthService) Refresh(ctx context.Context, refreshToken string) (token *models.AuthToken, err error) {
	err = s.transactor(ctx, func(ctxWithTx context.Context) error {
		token, err = s.AuthService.Refresh(ctx, refreshToken)

		return err
	})

	return token, err
}

func (s *AuthService) SignOut(ctx context.Context) (err error) {
	return s.transactor(ctx, func(ctxWithTx context.Context) error {
		return s.AuthService.SignOut(ctx)
	})
}
