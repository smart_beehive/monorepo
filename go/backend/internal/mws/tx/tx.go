package tx

import "context"

type Transactor func(ctx context.Context, fWithTx func(ctxWithTx context.Context) error) error
