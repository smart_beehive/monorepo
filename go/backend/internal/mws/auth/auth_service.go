package auth

import (
	"context"

	"bitbucket.org/smart_beehive/backend/internal"
	"bitbucket.org/smart_beehive/backend/internal/models"
)

type AuthService struct {
	internal.AuthService
}

func MakeForAuthService() internal.AuthServiceMW {
	return func(next internal.AuthService) internal.AuthService {
		return &AuthService{
			AuthService: next,
		}
	}
}

func (s *AuthService) SignOut(ctx context.Context) (_ error) {
	_, isExists := models.AuthTokenFromContext(ctx)
	if !isExists {
		return internal.ErrAuthTokenInvalid
	}

	return s.AuthService.SignOut(ctx)
}
