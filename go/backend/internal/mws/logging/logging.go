package logging

import "github.com/go-logr/logr"

func logError(l logr.Logger, err error, method string) {
	if err != nil {
		l.Error(err, method)
	}
}
