package logging

import (
	"context"

	internal "bitbucket.org/smart_beehive/backend/internal"
	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/go-logr/logr"
)

type AuthService struct {
	l    logr.Logger
	next internal.AuthService
}

func MakeForAuthService(l logr.Logger) internal.AuthServiceMW {
	l = l.WithName("AuthService")

	return func(next internal.AuthService) internal.AuthService {
		return &AuthService{
			l:    l,
			next: next,
		}
	}
}

func (s *AuthService) ConnectProvider(ctx context.Context, providerName models.OAuthProviderName) (redirectURLToProvider string, err error) {
	defer func() {
		logError(s.l, err, "ConnectProvider")
	}()

	return s.next.ConnectProvider(ctx, providerName)
}

func (s *AuthService) CallbackFromProvider(ctx context.Context, code string, state string) (redirectURLToToken string, err error) {
	defer func() {
		logError(s.l, err, "CallbackFromProvider")
	}()

	return s.next.CallbackFromProvider(ctx, code, state)
}

func (s *AuthService) Token(ctx context.Context, internalCode string) (token *models.AuthToken, err error) {
	defer func() {
		logError(s.l, err, "Token")
	}()

	return s.next.Token(ctx, internalCode)
}

func (s *AuthService) Refresh(ctx context.Context, refreshToken string) (token *models.AuthToken, err error) {
	defer func() {
		logError(s.l, err, "Refresh")
	}()

	return s.next.Refresh(ctx, refreshToken)
}

func (s *AuthService) SignOut(ctx context.Context) (err error) {
	defer func() {
		logError(s.l, err, "SignOut")
	}()

	return s.next.SignOut(ctx)
}
