package logging

import (
	"net/http"

	endpointshttp "bitbucket.org/smart_beehive/backend/internal/endpoints/http"
	"github.com/go-logr/logr"
)

func MakeForRequestReader(l logr.Logger) endpointshttp.RequestReaderMW {
	l = l.WithName("RequestReader")

	return func(next endpointshttp.RequestReader) endpointshttp.RequestReader {
		return func(r *http.Request, dst interface{}) (err error) {
			defer func() {
				logError(l, err, "RequestReader")
			}()

			return next(r, dst)
		}
	}
}

func MakeForResponseWriter(l logr.Logger) endpointshttp.ResponseWriterMW {
	l = l.WithName("ResponseWriter")

	return func(next endpointshttp.ResponseWriter) endpointshttp.ResponseWriter {
		return func(w http.ResponseWriter, src interface{}) (err error) {
			defer func() {
				logError(l, err, "ResponseWriter")
			}()

			return next(w, src)
		}
	}
}
