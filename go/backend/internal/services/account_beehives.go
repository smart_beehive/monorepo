package services

import (
	"context"
	"fmt"

	"bitbucket.org/smart_beehive/backend/internal"
	"bitbucket.org/smart_beehive/backend/internal/models"
	"bitbucket.org/smart_beehive/backend/internal/repositories"
	"bitbucket.org/smart_beehive/backend/internal/tools/encrypt"
)

type AccountBeeHivesService struct {
	beehives       repositories.BeeHives
	bindedBeehives repositories.BindedBeeHives
	bindedApiaries repositories.BindedApiaries

	privateKey string
}

func NewAccountBeeHivesService(
	beehives repositories.BeeHives,
	bindedBeehives repositories.BindedBeeHives,
	bindedApiaries repositories.BindedApiaries,
	privateKey string,
) *AccountBeeHivesService {
	return &AccountBeeHivesService{
		beehives:       beehives,
		bindedBeehives: bindedBeehives,
		bindedApiaries: bindedApiaries,
		privateKey:     privateKey,
	}
}

// Bind метод обрабатывает добавление улия в пасеку к аккаунту пользователя.
//
// На каждом улье есть QR код в котором находится идентификатор улья.
func (s *AccountBeeHivesService) Bind(ctx context.Context, qrCodeContent string, bindApiaryID string) (bindedBeehive *models.BindedBeeHive, err error) {
	authToken := models.MustAuthTokenFromContext(ctx)

	isOwner, err := s.bindedApiaries.CheckOwner(ctx, bindApiaryID, authToken.UID)
	if err != nil {
		return nil, fmt.Errorf("check owner apiary: %w", err)
	}

	if !isOwner {
		return nil, internal.ErrAccessDonied
	}

	beehiveID, err := encrypt.Decrypt(qrCodeContent, s.privateKey)
	if err != nil {
		return nil, internal.ErrBadQRCodeContent
	}

	beehive, err := s.beehives.ReadByID(ctx, beehiveID)
	if err != nil {
		return nil, fmt.Errorf("read beehive: %w", err)
	}

	if beehive == nil || beehive.DeletedAt != nil {
		return nil, internal.ErrBeeHiveNotFound
	}

	isBinding, err := s.bindedBeehives.CheckExists(ctx, beehive.ID, authToken.UID)
	if err != nil {
		return nil, fmt.Errorf("check exists bind: %w", err)
	}

	if isBinding {
		return nil, internal.ErrBeeHiveIsAlreadyBinded
	}

	bindedBeehive, err = s.bindedBeehives.Create(ctx, bindApiaryID, beehive.ID)
	if err != nil {
		return nil, fmt.Errorf("create bind: %w", err)
	}

	return bindedBeehive, nil
}

// Rebind метод выполняет передобавление улья в другую пасеку аккаунта пользователя.
func (s *AccountBeeHivesService) Rebind(ctx context.Context, id, newBindApiaryID string) (beehive *models.BindedBeeHive, err error) {
	authToken := models.MustAuthTokenFromContext(ctx)

	isOwner, err := s.bindedBeehives.CheckOwner(ctx, id, authToken.UID)
	if err != nil {
		return nil, fmt.Errorf("check binded beehive owner: %w", err)
	}

	if !isOwner {
		return nil, internal.ErrAccessDonied
	}

	isOwner, err = s.bindedApiaries.CheckOwner(ctx, newBindApiaryID, authToken.UID)
	if err != nil {
		return nil, fmt.Errorf("check binded apiary owner: %w", err)
	}

	if !isOwner {
		return nil, internal.ErrAccessDonied
	}

	beehive, err = s.bindedBeehives.Rebind(ctx, id, newBindApiaryID)
	if err != nil {
		return nil, fmt.Errorf("rebind beehive: %w", err)
	}

	return beehive, nil
}

// Unbind метод обрабатывает удаление улья из пасеки из аккаунта пользователя.
func (s *AccountBeeHivesService) Unbind(ctx context.Context, id string) (err error) {
	authToken := models.MustAuthTokenFromContext(ctx)

	isOwner, err := s.bindedBeehives.CheckOwner(ctx, id, authToken.UID)
	if err != nil {
		return fmt.Errorf("check binded beehive owner: %w", err)
	}

	if !isOwner {
		return internal.ErrAccessDonied
	}

	err = s.bindedBeehives.Delete(ctx, id)
	if err != nil {
		return fmt.Errorf("delete binded beehive: %w", err)
	}

	return nil
}

// Read метод возвращает данные об улье.
func (s *AccountBeeHivesService) Read(ctx context.Context, id string) (beehive *models.BindedBeeHive, err error) {
	authToken := models.MustAuthTokenFromContext(ctx)

	isOwner, err := s.bindedBeehives.CheckOwner(ctx, id, authToken.UID)
	if err != nil {
		return nil, fmt.Errorf("check binded beehive owner: %w", err)
	}

	if !isOwner {
		return nil, internal.ErrAccessDonied
	}

	return s.bindedBeehives.ReadByID(ctx, id)
}

// ReadPage метод возвращает страницу с данными об улье, где count общее кол-во записей.
func (s *AccountBeeHivesService) ReadPage(ctx context.Context, bindApiaryID string, pageNumber int) (count int, beehives []*models.BindedBeeHive, err error) {
	authToken := models.MustAuthTokenFromContext(ctx)

	isOwner, err := s.bindedApiaries.CheckOwner(ctx, bindApiaryID, authToken.UID)
	if err != nil {
		return 0, nil, fmt.Errorf("check owner apiary: %w", err)
	}

	if !isOwner {
		return 0, nil, internal.ErrAccessDonied
	}

	beehives, err = s.bindedBeehives.ReadPage(ctx, bindApiaryID, pageNumber)
	if err != nil {
		return 0, nil, fmt.Errorf("read binded beehives page: %w", err)
	}

	count, err = s.bindedBeehives.Count(ctx, bindApiaryID)
	if err != nil {
		return 0, nil, fmt.Errorf("count binded beehives: %w", err)
	}

	return count, beehives, nil
}
