package services

import (
	"bytes"
	"context"
	"fmt"
	"net/url"
	"time"

	"bitbucket.org/smart_beehive/backend/internal"
	"bitbucket.org/smart_beehive/backend/internal/models"
	"bitbucket.org/smart_beehive/backend/internal/repositories"
	"golang.org/x/oauth2"
)

type OAuthCredentionals struct {
	ClientID     string `required:"true" split_words:"true" desc:"идентификатор клиента oauth провайдера."`
	ClientSecret string `required:"true" split_words:"true" desc:"токен клиента oauth провайдера."`
}

type OAuthProvider interface {
	Name() models.OAuthProviderName
	Config() *oauth2.Config
	User(ctx context.Context, t *oauth2.Token) (externalUser *models.OAuthUser, err error)
}

type AuthServiceConfig struct {
	OAuthConnectSessionLifePeriod  time.Duration `split_words:"true" default:"5m" desc:"время жизни сессии для подключения провайдера."`
	ExchangeCodeForTokenLifePeriod time.Duration `split_words:"true" default:"5m" desc:"время отведенное на обмен кода на токен."`
	EndpointTokenURL               string        `required:"true" split_words:"true" desc:"url на который надо перенаправить клиента для обмена кода на токен."`
	AccessTokenLifePeriod          time.Duration `split_words:"true" default:"12h" desc:"время жизни токена доступа, проставляет в момент создания."`
	RefreshTokenLifePeriod         time.Duration `split_words:"true" default:"48h" desc:"время жизни токена обновления, проставляет в момент создания."`
}

type AuthService struct {
	oauthConnectProviderSessions repositories.OAuthConnectProviderSessions
	externalUsers                repositories.ExternalUsers
	users                        repositories.Users
	exchangeCodeForTokens        repositories.ExchangeCodeForTokens
	devices                      repositories.Devices
	authTokens                   repositories.AuthTokens

	providers map[models.OAuthProviderName]OAuthProvider

	config AuthServiceConfig
}

func NewAuthService(
	oauthConnectProviderSessions repositories.OAuthConnectProviderSessions,
	externalUsers repositories.ExternalUsers,
	users repositories.Users,
	exchangeCodeForTokens repositories.ExchangeCodeForTokens,
	devices repositories.Devices,
	authTokens repositories.AuthTokens,
	providers []OAuthProvider,
	config AuthServiceConfig,
) *AuthService {
	providersMap := make(map[models.OAuthProviderName]OAuthProvider, len(providers))

	for _, p := range providers {
		providersMap[p.Name()] = p
	}

	return &AuthService{
		oauthConnectProviderSessions: oauthConnectProviderSessions,
		externalUsers:                externalUsers,
		users:                        users,
		exchangeCodeForTokens:        exchangeCodeForTokens,
		devices:                      devices,
		authTokens:                   authTokens,
		providers:                    providersMap,
		config:                       config,
	}
}

// ConnectProvider метод создает OAuthConnectProviderSession и формирует ссылку для редиректа на oauth провайдера.
//
// 1. Проверка что провайдер поддерживается;
// 2. Создания OAuthConnectSession через репозиторий;
// 3. Формирование ссылки для редиректа на провайдера.
//
// Возвращает следующие ошибки: ErrAuthProviderNotSupported.
func (s *AuthService) ConnectProvider(ctx context.Context, providerName models.OAuthProviderName) (redirectURLToProvider string, err error) {
	provider, isExists := s.providers[providerName]
	if !isExists {
		return "", internal.ErrAuthProviderNotSupported
	}

	clientClaims := models.MustClientClaimsFromCtx(ctx)

	session, err := s.oauthConnectProviderSessions.Create(
		ctx,
		clientClaims,
		provider.Name(),
		time.Now().Add(s.config.OAuthConnectSessionLifePeriod),
	)
	if err != nil {
		return "", fmt.Errorf("s.oauthConnectProviderSessions.Create: %w", err)
	}

	redirectURLToProvider = provider.Config().AuthCodeURL(session.ID)

	return redirectURLToProvider, nil
}

// CallbackFromProvider метод выполняет обработку редиректа от oauth провайдера.
//
// 1. Извлечение OAuthConnectProviderSession;
// 2. Проверка того что она существует и еще актуальная;
// 3. Проверка что провайдер поддерживается;
// 4. Обмен code на токен у провайдера;
// 5. Запрос User от провайдера;
// 6. Извлечение пользователя из UsersRepository;
// 7. Если пользователя нет, то создать из User;
// 8. Создать ExchangeCodeForToken;
// 9. Собрать и вернуть ссылку редиректа на точку обмена кода на токен.
//
// Возвращает следующие ошибки: ErrOAuthConnectSessionNotExists, ErrOAuthConnectSessionExpired, ErrAuthProviderNotSupported.
func (s *AuthService) CallbackFromProvider(ctx context.Context, code, state string) (redirectURLToToken string, err error) {
	clientClaims := models.MustClientClaimsFromCtx(ctx)

	session, err := s.oauthConnectProviderSessions.ReadByIDAndClientClaims(ctx, state, clientClaims)
	if err != nil {
		return "", fmt.Errorf("s.oauthConnectProviderSessions.ReadByIDAndClientClaims: %w", err)
	}

	if session == nil {
		return "", internal.ErrOAuthConnectSessionNotExists
	}

	if time.Now().After(session.ExpiredAfter) {
		return "", internal.ErrOAuthConnectSessionExpired
	}

	provider, isExists := s.providers[session.ProviderName]
	if !isExists {
		return "", internal.ErrAuthProviderNotSupported
	}

	token, err := provider.Config().Exchange(ctx, code)
	if err != nil {
		return "", fmt.Errorf("provider(%s).Config().Exchange: %w", session.ProviderName, err)
	}

	oauthUser, err := provider.User(ctx, token)
	if err != nil {
		return "", fmt.Errorf("provider(%s).User: %w", session.ProviderName, err)
	}

	externalUser, err := s.externalUsers.ReadByExernalID(ctx, oauthUser.ID, provider.Name())
	if err != nil {
		return "", fmt.Errorf("s.externalUsers.ReadByExernalID: %w", err)
	}

	var internalUser *models.User

	if externalUser != nil {
		internalUser, err = s.users.ReadByExternalUser(ctx, provider.Name(), externalUser.ExternalUID)
		if err != nil {
			return "", fmt.Errorf("s.users.ReadByExternalUser: %w", err)
		}

		err = s.externalUsers.UpdateToken(ctx, externalUser, token)
		if err != nil {
			return "", fmt.Errorf("s.externalUsers.UpdateToken: %w", err)
		}
	} else {
		internalUser, err = s.users.CreateByOAuthUser(ctx, *oauthUser)
		if err != nil {
			return "", fmt.Errorf("s.users.CreateByExternalUser: %w", err)
		}

		externalUser, err = s.externalUsers.Create(ctx, internalUser.ID, oauthUser.ID, provider.Name(), token)
		if err != nil {
			return "", fmt.Errorf("s.externalUsers.Create: %w", err)
		}
	}

	exchangeCodeForToken, err := s.exchangeCodeForTokens.Create(
		ctx,
		internalUser.ID,
		clientClaims,
		time.Now().Add(s.config.ExchangeCodeForTokenLifePeriod),
	)
	if err != nil {
		return "", fmt.Errorf("s.exchangeCodeForTokens.Create: %w", err)
	}

	var redirectURLToTokenBuf bytes.Buffer

	redirectURLToTokenBuf.WriteString(s.config.EndpointTokenURL)
	redirectURLToTokenBuf.WriteString("?")
	redirectURLToTokenBuf.WriteString(url.Values{internal.RedirectURLToTokenValueCodeName: {exchangeCodeForToken.ID}}.Encode())

	return redirectURLToTokenBuf.String(), nil
}

// Token метод выполняет замену code на AuthToken.
//
// 1. Из репозирования извлекается ExchangeCodeForToken;
// 2. Проверяется его существование и актуальность;
// 3. Извлекается device с которого пользователь проходил механизм авторизации;
// 4. Если device нет, то создаем через репозиторий;
// 5. Создаем через репозиторий AuthToken.
//
// Возвращает следующие ошибки: ErrExchangeCodeForTokenNotExists, ErrExchangeCodeForTokenExpired.
func (s *AuthService) Token(ctx context.Context, code string) (token *models.AuthToken, err error) {
	clientClaims := models.MustClientClaimsFromCtx(ctx)

	exchangeCodeForToken, err := s.exchangeCodeForTokens.ReadByIDAndClientClaims(ctx, code, clientClaims)
	if err != nil {
		return nil, fmt.Errorf("s.exchangeCodeForTokens.ReadByIDAndClientClaims: %w", err)
	}

	if exchangeCodeForToken == nil {
		return nil, internal.ErrExchangeCodeForTokenNotExists
	}

	if time.Now().After(exchangeCodeForToken.ExpiredAfter) {
		return nil, internal.ErrExchangeCodeForTokenExpired
	}

	uid := exchangeCodeForToken.UID

	device, err := s.devices.ReadByUIDAndClientClaims(ctx, uid, clientClaims)
	if err != nil {
		return nil, fmt.Errorf("s.devices.ReadByUIDAndClientClaims: %w", err)
	}

	if device == nil {
		device, err = s.devices.Create(ctx, uid, clientClaims)
		if err != nil {
			return nil, fmt.Errorf("s.devices.Create: %w", err)
		}
	}

	authToken, err := s.authTokens.Create(
		ctx,
		uid,
		device.ID,
		time.Now().Add(s.config.AccessTokenLifePeriod),
		time.Now().Add(s.config.RefreshTokenLifePeriod),
	)
	if err != nil {
		return nil, fmt.Errorf("s.authTokens.Create: %w", err)
	}

	return authToken, nil
}

// Refresh метод выполняет обновление токена доступа по токену обновления.
//
// 1. Извлечение токена из репозитория;
// 2. Проверка что токен существует;
// 3. Проверка что токен обновления еще актуальный;
// 4. Создание нового токена доступа и обновления через репозиторий.
//
// Возвращает следующие ошибки: ErrAuthTokenNotExists, ErrRefreshTokenExpired.
func (s *AuthService) Refresh(ctx context.Context, refreshToken string) (token *models.AuthToken, err error) {
	authToken, err := s.authTokens.ReadByRefreshToken(ctx, refreshToken)
	if err != nil {
		return nil, fmt.Errorf("s.authTokens.ReadByRefreshToken: %w", err)
	}

	if authToken == nil {
		return nil, internal.ErrAuthTokenNotExists
	}

	if !authToken.IsRefreshTokenValid() {
		return nil, internal.ErrRefreshTokenInvalid
	}

	err = s.authTokens.Disable(ctx, authToken.AccessToken)
	if err != nil {
		return nil, fmt.Errorf("s.authTokens.Disable: %w", err)
	}

	newAuthToken, err := s.authTokens.Create(
		ctx,
		authToken.UID,
		authToken.DID,
		time.Now().Add(s.config.AccessTokenLifePeriod),
		time.Now().Add(s.config.RefreshTokenLifePeriod),
	)
	if err != nil {
		return nil, fmt.Errorf("s.authTokens.Create: %w", err)
	}

	return newAuthToken, nil
}

// SignOut метод извлекает из контекста токен доступа и выполняет его диактивацию.
//
// 1. Извлеч из контекста токен доступа;
// 2. Выполнить диактивацию через репозиторий.
func (s *AuthService) SignOut(ctx context.Context) error {
	authToken := models.MustAuthTokenFromContext(ctx)

	err := s.authTokens.Disable(ctx, authToken.AccessToken)
	if err != nil {
		return fmt.Errorf("s.authTokens.Disable: %w", err)
	}

	return nil
}
