package services

import (
	"context"
	"fmt"

	"bitbucket.org/smart_beehive/backend/internal/models"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	option "google.golang.org/api/option"
	"google.golang.org/api/people/v1"
)

type GoogleOAuthProvider struct {
	config *oauth2.Config
}

func NewGoogleOAuthProvider(redirectURL string, creds OAuthCredentionals) *GoogleOAuthProvider {
	return &GoogleOAuthProvider{
		config: &oauth2.Config{
			ClientID:     creds.ClientID,
			ClientSecret: creds.ClientSecret,
			RedirectURL:  redirectURL,
			Scopes: []string{
				people.UserinfoEmailScope,
				people.UserinfoProfileScope,
			},
			Endpoint: google.Endpoint,
		},
	}
}

func (p *GoogleOAuthProvider) Name() models.OAuthProviderName {
	return models.GoogleOAuthProvider
}

func (p *GoogleOAuthProvider) Config() *oauth2.Config {
	return p.config
}

func (p *GoogleOAuthProvider) User(ctx context.Context, t *oauth2.Token) (externalUser *models.OAuthUser, err error) {
	peopleService, err := people.NewService(ctx, option.WithTokenSource(p.config.TokenSource(ctx, t)))
	if err != nil {
		return nil, fmt.Errorf("people.NewService: %w", err)
	}

	person, err := peopleService.People.Get("people/me").PersonFields("emailAddresses,names").Do()
	if err != nil {
		return nil, fmt.Errorf("peopleService.People.Get(\"people/me\").Do(): %w", err)
	}

	name := person.Names[0]
	email := person.EmailAddresses[0].Value

	return &models.OAuthUser{
		ID:         person.ResourceName,
		FirstName:  name.GivenName,
		MiddleName: name.MiddleName,
		LastName:   name.FamilyName,
		Email:      email,
	}, nil
}
