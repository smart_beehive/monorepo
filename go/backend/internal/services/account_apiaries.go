package services

import (
	"context"
	"fmt"

	"bitbucket.org/smart_beehive/backend/internal"
	"bitbucket.org/smart_beehive/backend/internal/models"
	"bitbucket.org/smart_beehive/backend/internal/repositories"
	"bitbucket.org/smart_beehive/backend/internal/tools/encrypt"
)

type AccountApiariesService struct {
	apiaries       repositories.Apiaries
	bindedApiaries repositories.BindedApiaries
	bindedBeehives repositories.BindedBeeHives

	privateKey string
}

func NewAccountApiariesService(
	apiaries repositories.Apiaries,
	bindedApiaries repositories.BindedApiaries,
	bindedBeehives repositories.BindedBeeHives,
	privateKey string,
) *AccountApiariesService {
	return &AccountApiariesService{
		apiaries:       apiaries,
		bindedApiaries: bindedApiaries,
		bindedBeehives: bindedBeehives,
		privateKey:     privateKey,
	}
}

// Bind метод обрабатывает добавление пасеки к аккаунту пользователя.
//
// На каждой пасеке есть QR код в котором находится идентификатор пасеки.
func (s *AccountApiariesService) Bind(ctx context.Context, qrCodeContent string, name string) (userApiary *models.BindedApiary, err error) {
	authToken := models.MustAuthTokenFromContext(ctx)

	apiaryID, err := encrypt.Decrypt(qrCodeContent, s.privateKey)
	if err != nil {
		return nil, internal.ErrBadQRCodeContent
	}

	apiary, err := s.apiaries.ReadByID(ctx, apiaryID)
	if err != nil {
		return nil, fmt.Errorf("read apiary by id: %w", err)
	}

	if apiary == nil || apiary.DeletedAt != nil {
		return nil, internal.ErrApiaryNotFound
	}

	isBinding, err := s.bindedApiaries.CheckExists(ctx, apiary.ID, authToken.UID)
	if err != nil {
		return nil, fmt.Errorf("check exists bind: %w", err)
	}

	if isBinding {
		return nil, internal.ErrApiaryIsAlreadyBinded
	}

	bindedApiary, err := s.bindedApiaries.Create(ctx, apiary.ID, authToken.UID, name)
	if err != nil {
		return nil, fmt.Errorf("create bind: %w", err)
	}

	return bindedApiary, nil
}

// Rename переименовать пасеку.
func (s *AccountApiariesService) Rename(ctx context.Context, id string, name string) (err error) {
	apiary, err := s.Read(ctx, id)
	if err != nil {
		return fmt.Errorf("read binded apiary: %w", err)
	}

	err = s.bindedApiaries.UpdateName(ctx, apiary.ID, name)
	if err != nil {
		return fmt.Errorf("update user apiary name: %w", err)
	}

	return nil
}

// RebindBeeHeevies метод переносит все ульи на другую пасеку.
func (s *AccountApiariesService) RebindBeeHives(ctx context.Context, id, newBindApiary string) (err error) {
	apiary, err := s.Read(ctx, id)
	if err != nil {
		return fmt.Errorf("read binded apiary: %w", err)
	}

	newApiary, err := s.Read(ctx, newBindApiary)
	if err != nil {
		return fmt.Errorf("read new binded apiary: %w", err)
	}

	err = s.bindedBeehives.RebindAll(ctx, apiary.ID, newApiary.ID)
	if err != nil {
		return fmt.Errorf("rebind all beehives: %w", err)
	}

	return nil
}

// UnbindBeeHives отвязывает все ульи от пасеки.
func (s *AccountApiariesService) UnbindBeeHives(ctx context.Context, id string) (err error) {
	apiary, err := s.Read(ctx, id)
	if err != nil {
		return fmt.Errorf("read binded apiary: %w", err)
	}

	return s.bindedBeehives.DeleteAllByBindApiaryID(ctx, apiary.ID)
}

// Unbind метод обрабатывает удаление пасеки из аккаунта пользователя.
func (s *AccountApiariesService) Unbind(ctx context.Context, id string) (err error) {
	apiary, err := s.Read(ctx, id)
	if err != nil {
		return fmt.Errorf("read binded apiary: %w", err)
	}

	count, err := s.bindedBeehives.Count(ctx, apiary.ID)
	if err != nil {
		return fmt.Errorf("count binded ")
	}

	if count != 0 {
		return internal.ErrApiaryNotUnbindMustRebindBeehives
	}

	err = s.bindedApiaries.Delete(ctx, apiary.ID)
	if err != nil {
		return fmt.Errorf("delete bind: %w", err)
	}

	return nil
}

// Read метод возвращает данные об пасеке.
func (s *AccountApiariesService) Read(ctx context.Context, id string) (beehive *models.BindedApiary, err error) {
	authToken := models.MustAuthTokenFromContext(ctx)

	apiary, err := s.bindedApiaries.ReadByID(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("read apiary by id: %w", err)
	}

	if apiary == nil {
		return nil, internal.ErrApiaryNotFound
	}

	if apiary.UID != authToken.UID {
		return nil, internal.ErrAccessDonied
	}

	return apiary, nil
}

// ReadPage метод возвращает страницу с данными об пасеках, где count это общее кол-во записей.
func (s *AccountApiariesService) ReadPage(ctx context.Context, pageNumber int) (count int, apiaries []*models.BindedApiary, err error) {
	authToken := models.MustAuthTokenFromContext(ctx)

	apiaries, err = s.bindedApiaries.ReadPage(ctx, authToken.UID, pageNumber)
	if err != nil {
		return 0, nil, fmt.Errorf("read binded apiaries page: %w", err)
	}

	count, err = s.bindedApiaries.Count(ctx, authToken.UID)
	if err != nil {
		return 0, nil, fmt.Errorf("count binded apiaries: %w", err)
	}

	return count, apiaries, nil
}
