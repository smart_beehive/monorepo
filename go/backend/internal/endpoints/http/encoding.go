package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type RequestReader func(r *http.Request, dst interface{}) error

type RequestReaderMW func(next RequestReader) RequestReader

func WrapRequestReader(f RequestReader, mws ...RequestReaderMW) RequestReader {
	for _, mw := range mws {
		f = mw(f)
	}

	return f
}

type ResponseWriter func(w http.ResponseWriter, src interface{}) error

type ResponseWriterMW func(next ResponseWriter) ResponseWriter

func WrapResponseWriter(f ResponseWriter, mws ...ResponseWriterMW) ResponseWriter {
	for _, mw := range mws {
		f = mw(f)
	}

	return f
}

func ResponseWriterReader(f ResponseWriter, mws ...ResponseWriterMW) ResponseWriter {
	for _, mw := range mws {
		f = mw(f)
	}

	return f
}

const (
	JSONContentTypeHeader = "Content-Type"
	JSONContentType       = "application/json"
)

func ReadJSON(r *http.Request, dst interface{}) error {
	defer r.Body.Close()

	contentType := r.Header.Get(JSONContentTypeHeader)

	if contentType == JSONContentType {
		return NewError(
			http.StatusUnsupportedMediaType,
			fmt.Sprintf("%s is not %s", contentType, JSONContentType),
		)
	}

	err := json.NewDecoder(r.Body).Decode(dst)
	if err != nil {
		return NewErrorWithErrMsg(http.StatusBadRequest, err)
	}

	return nil
}

func WriteJSON(w http.ResponseWriter, src interface{}) error {
	var buf bytes.Buffer

	encoder := json.NewEncoder(&buf)

	err := encoder.Encode(src)
	if err != nil {
		return fmt.Errorf("encoder.Encode: %w", err)
	}

	_, err = w.Write(buf.Bytes())
	if err != nil {
		return fmt.Errorf("w.Write: %w", err)
	}

	w.Header().Add("Content-Type", "application/json")

	return nil
}
