package http

import (
	"errors"
	"net/http"

	"bitbucket.org/smart_beehive/backend/internal"
	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/gorilla/mux"
)

type AuthServiceEndpoints struct {
	*EndpointsBase

	svc internal.AuthService
}

func NewAuthServiceEndpoints(base *EndpointsBase, svc internal.AuthService) *AuthServiceEndpoints {
	return &AuthServiceEndpoints{
		EndpointsBase: base,
		svc:           svc,
	}
}

func (e *AuthServiceEndpoints) ConnectProvider(w http.ResponseWriter, r *http.Request) {
	values := mux.Vars(r)

	provider := models.OAuthProviderName(values["provider"])

	redirectURLToProvider, err := e.svc.ConnectProvider(r.Context(), provider)
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}

	http.Redirect(w, r, redirectURLToProvider, http.StatusFound)
}

func (e *AuthServiceEndpoints) CallbackFromProvider(w http.ResponseWriter, r *http.Request) {
	values := r.URL.Query()

	code := values.Get("code")
	state := values.Get("state")

	redirectURLToToken, err := e.svc.CallbackFromProvider(r.Context(), code, state)
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}

	http.Redirect(w, r, redirectURLToToken, http.StatusFound)
}

func (e *AuthServiceEndpoints) Token(w http.ResponseWriter, r *http.Request) {
	values := r.URL.Query()

	internalCode := values.Get(internal.RedirectURLToTokenValueCodeName)

	token, err := e.svc.Token(r.Context(), internalCode)
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}

	if err = e.responseWriter(w, token); err != nil {
		_ = ErrInternal.WriteToResponse(w)
	}
}

func (e *AuthServiceEndpoints) Refresh(w http.ResponseWriter, r *http.Request) {
	values := r.URL.Query()

	refreshToken := values.Get("refreshToken")

	token, err := e.svc.Refresh(r.Context(), refreshToken)
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}

	if err = e.responseWriter(w, token); err != nil {
		_ = ErrInternal.WriteToResponse(w)
	}
}

func (e *AuthServiceEndpoints) SignOut(w http.ResponseWriter, r *http.Request) {
	err := e.svc.SignOut(r.Context())
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}
}

func (e *AuthServiceEndpoints) mappingServiceError(err error) (httpError *Error) {
	if errors.Is(err, internal.ErrAuthProviderNotSupported) {
		return NewErrorWithErrMsg(http.StatusBadRequest, err)
	}

	if errors.Is(err, internal.ErrOAuthConnectSessionNotExists) {
		return NewErrorWithErrMsg(http.StatusNotFound, err)
	}

	if errors.Is(err, internal.ErrOAuthConnectSessionExpired) {
		return NewErrorWithErrMsg(http.StatusForbidden, err)
	}

	if errors.Is(err, internal.ErrExchangeCodeForTokenNotExists) {
		return NewErrorWithErrMsg(http.StatusNotFound, err)
	}

	if errors.Is(err, internal.ErrExchangeCodeForTokenExpired) {
		return NewErrorWithErrMsg(http.StatusForbidden, err)
	}

	if errors.Is(err, internal.ErrAuthTokenNotExists) {
		return NewErrorWithErrMsg(http.StatusNotFound, err)
	}

	if errors.Is(err, internal.ErrRefreshTokenInvalid) {
		return NewErrorWithErrMsg(http.StatusForbidden, err)
	}

	if errors.Is(err, internal.ErrAuthTokenInvalid) {
		return NewErrorWithErrMsg(http.StatusUnauthorized, err)
	}

	return nil
}
