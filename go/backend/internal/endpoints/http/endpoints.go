package http

type EndpointsBase struct {
	requestReader  RequestReader
	responseWriter ResponseWriter
}

func NewEndpointsBase(requestReader RequestReader, responseWriter ResponseWriter) *EndpointsBase {
	return &EndpointsBase{
		requestReader:  requestReader,
		responseWriter: responseWriter,
	}
}
