package http

import (
	"errors"
	"fmt"
	"net/http"

	"bitbucket.org/smart_beehive/backend/internal"
)

var ErrInternal = NewError(http.StatusInternalServerError, "internal server error")

type Error struct {
	Code int
	Msg  string
}

func NewError(code int, msg string) *Error {
	return &Error{
		Code: code,
		Msg:  msg,
	}
}

func NewErrorWithErrMsg(code int, err error) *Error {
	if err == nil {
		return nil
	}

	if unwrapErr := errors.Unwrap(err); unwrapErr != nil {
		err = unwrapErr
	}

	return &Error{
		Code: code,
		Msg:  err.Error(),
	}
}

func NewErrorAsError(err error) *Error {
	var httpError *Error

	if errors.As(err, &httpError) {
		return httpError
	}

	return nil
}

func (e *Error) WriteToResponse(w http.ResponseWriter) error {
	w.WriteHeader(e.Code)

	_, err := w.Write([]byte(e.Msg))
	if err != nil {
		return fmt.Errorf("w.Write: %w", err)
	}

	return nil
}

func (e *Error) String() string {
	return fmt.Sprintf("%d: %s", e.Code, e.Msg)
}

func (e *Error) Error() string {
	return e.String()
}

func procHandleErrorFromService(w http.ResponseWriter, err error, mapper func(err error) *Error) (stop bool) {
	if err == nil {
		return false
	}

	var httpErr *Error

	switch {
	case errors.As(err, &httpErr):
	case errors.Is(err, internal.ErrAccessDonied):
		httpErr = NewError(http.StatusForbidden, err.Error())
	case mapper != nil:
		httpErr = mapper(err)
	}

	if httpErr == nil {
		httpErr = ErrInternal
	}

	httpErr.WriteToResponse(w)

	return true
}
