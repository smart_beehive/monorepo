package http

import (
	"context"
	"net/http"
	"strings"

	"bitbucket.org/smart_beehive/backend/internal"
	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/gorilla/mux"
)

type AuthTokenExporter func(ctx context.Context, accessToken string) (*models.AuthToken, error)

type AuthTokenPassingToContext struct {
	exporter AuthTokenExporter
	typeName string
	next     http.Handler
}

func MakeAuthTokenToRequestContext(exporter AuthTokenExporter, typeName string) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return &AuthTokenPassingToContext{
			exporter: exporter,
			typeName: typeName,
			next:     next,
		}
	}
}

func (p *AuthTokenPassingToContext) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	authValue := r.Header.Get("Authorization")
	if authValue == "" {
		p.next.ServeHTTP(w, r)

		return
	}

	ctx := r.Context()

	accessToken := strings.TrimPrefix(authValue, p.typeName+" ")

	authToken, err := p.exporter(ctx, accessToken)
	if procHandleErrorFromService(w, err, nil) {
		return
	}

	if authToken == nil {
		p.next.ServeHTTP(w, r)

		return
	}

	if !authToken.IsAccessTokenValid() {
		NewErrorWithErrMsg(http.StatusUnauthorized, internal.ErrAuthTokenInvalid).
			WriteToResponse(w)

		return
	}

	r = r.WithContext(authToken.ToCtx(ctx))

	p.next.ServeHTTP(w, r)
}
