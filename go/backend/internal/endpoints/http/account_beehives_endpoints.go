package http

import (
	"errors"
	"net/http"
	"strconv"

	"bitbucket.org/smart_beehive/backend/internal"
	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/gorilla/mux"
)

type AccountBeeHivesServiceEndpoints struct {
	*EndpointsBase

	svc internal.AccountBeeHivesService
}

func NewAccountBeeHiveServiceEndpoints(base *EndpointsBase, svc internal.AccountBeeHivesService) *AccountBeeHivesServiceEndpoints {
	return &AccountBeeHivesServiceEndpoints{
		EndpointsBase: base,
		svc:           svc,
	}
}

func (e *AccountBeeHivesServiceEndpoints) Bind(w http.ResponseWriter, r *http.Request) {
	type request struct {
		QRCodeContent string `json:"qr_code_content"`
		BindApiaryID  string `json:"bind_apiary_id"`
	}

	req := new(request)

	if err := e.requestReader(r, req); err != nil {
		procHandleErrorFromService(w, err, nil)

		return
	}

	beehive, err := e.svc.Bind(r.Context(), req.QRCodeContent, req.BindApiaryID)
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}

	err = e.responseWriter(w, beehive)
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}
}

func (e *AccountBeeHivesServiceEndpoints) Rebind(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	beehiveID := vars["beehiveID"]

	type request struct {
		NewApiaryID string `json:"new_apiary_id"`
	}

	req := new(request)

	if err := e.requestReader(r, req); err != nil {
		procHandleErrorFromService(w, err, nil)

		return
	}

	beehive, err := e.svc.Rebind(r.Context(), req.NewApiaryID, beehiveID)
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}

	if err = e.responseWriter(w, beehive); err != nil {
		_ = ErrInternal.WriteToResponse(w)
	}
}

func (e *AccountBeeHivesServiceEndpoints) Unbind(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	beehiveID := vars["beehiveID"]

	err := e.svc.Unbind(r.Context(), beehiveID)
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}
}

func (e *AccountBeeHivesServiceEndpoints) Read(w http.ResponseWriter, r *http.Request) {
	values := mux.Vars(r)

	beehive, err := e.svc.Read(r.Context(), values["bindID"])
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}

	if err = e.responseWriter(w, beehive); err != nil {
		_ = ErrInternal.WriteToResponse(w)
	}
}

func (e *AccountBeeHivesServiceEndpoints) ReadPage(w http.ResponseWriter, r *http.Request) {
	values := mux.Vars(r)

	bindApiaryID := values["bindApiaryID"]

	pageNumber, err := strconv.Atoi(values["pageNumber"])
	if err != nil {
		_ = NewError(http.StatusBadRequest, "pageNumber mus bee number").WriteToResponse(w)
		return
	}

	count, beehives, err := e.svc.ReadPage(r.Context(), bindApiaryID, pageNumber)
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}

	type response struct {
		Count    int                     `json:"count"`
		Beehives []*models.BindedBeeHive `json:"beehives"`
	}

	res := &response{
		Count:    count,
		Beehives: beehives,
	}

	if err = e.responseWriter(w, res); err != nil {
		_ = ErrInternal.WriteToResponse(w)
	}
}

func (e *AccountBeeHivesServiceEndpoints) mappingServiceError(err error) (httpError *Error) {
	if errors.Is(err, internal.ErrApiaryNotFound) {
		return NewError(http.StatusNotFound, err.Error())
	}

	if errors.Is(err, internal.ErrBeeHiveNotFound) {
		return NewError(http.StatusNotFound, err.Error())
	}

	if errors.Is(err, internal.ErrBeeHiveIsAlreadyBinded) {
		return NewError(http.StatusConflict, err.Error())
	}

	if errors.Is(err, internal.ErrBadQRCodeContent) {
		return NewError(http.StatusBadRequest, err.Error())
	}

	return nil
}
