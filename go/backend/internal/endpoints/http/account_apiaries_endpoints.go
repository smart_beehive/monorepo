package http

import (
	"errors"
	"net/http"
	"strconv"

	"bitbucket.org/smart_beehive/backend/internal"
	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/gorilla/mux"
)

type AccountApiariesServiceEndpoints struct {
	*EndpointsBase

	svc internal.AccountApiariesService
}

func NewAccountApiariesServiceEndpoints(base *EndpointsBase, svc internal.AccountApiariesService) *AccountApiariesServiceEndpoints {
	return &AccountApiariesServiceEndpoints{
		EndpointsBase: base,
		svc:           svc,
	}
}

func (e *AccountApiariesServiceEndpoints) Bind(w http.ResponseWriter, r *http.Request) {
	type request struct {
		QRCodeContent string `json:"qr_code_content"`
		Name          string `json:"name"`
	}

	req := new(request)

	if err := e.requestReader(r, req); err != nil {
		procHandleErrorFromService(w, err, nil)

		return
	}

	apiary, err := e.svc.Bind(r.Context(), req.QRCodeContent, req.Name)
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}

	err = e.responseWriter(w, apiary)
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}
}

func (e *AccountApiariesServiceEndpoints) Rename(w http.ResponseWriter, r *http.Request) {
	values := mux.Vars(r)

	type request struct {
		Name string `json:"name"`
	}

	req := new(request)

	if err := e.requestReader(r, req); err != nil {
		procHandleErrorFromService(w, err, nil)

		return
	}

	err := e.svc.Rename(r.Context(), values["id"], req.Name)
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}
}

func (e *AccountApiariesServiceEndpoints) RebindBeeHeevies(w http.ResponseWriter, r *http.Request) {
	values := mux.Vars(r)

	err := e.svc.RebindBeeHives(r.Context(), values["id"], values["newBindApiaryID"])
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}
}

func (e *AccountApiariesServiceEndpoints) UnbindBeeHives(w http.ResponseWriter, r *http.Request) {
	values := mux.Vars(r)

	err := e.svc.UnbindBeeHives(r.Context(), values["id"])
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}
}

func (e *AccountApiariesServiceEndpoints) Unbind(w http.ResponseWriter, r *http.Request) {
	values := mux.Vars(r)

	err := e.svc.Unbind(r.Context(), values["id"])
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}
}

func (e *AccountApiariesServiceEndpoints) Read(w http.ResponseWriter, r *http.Request) {
	values := mux.Vars(r)

	apiary, err := e.svc.Read(r.Context(), values["id"])
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}

	if err = e.responseWriter(w, apiary); err != nil {
		_ = ErrInternal.WriteToResponse(w)
	}
}

func (e *AccountApiariesServiceEndpoints) ReadPage(w http.ResponseWriter, r *http.Request) {
	values := mux.Vars(r)
	pageNumber, err := strconv.Atoi(values["pageNumber"])
	if err != nil {
		_ = NewError(http.StatusBadRequest, "pageNumber mus bee number").WriteToResponse(w)
		return
	}

	count, apiaries, err := e.svc.ReadPage(r.Context(), pageNumber)
	if procHandleErrorFromService(w, err, e.mappingServiceError) {
		return
	}

	type response struct {
		Count    int                    `json:"count"`
		Apiaries []*models.BindedApiary `json:"apiaries"`
	}

	res := &response{
		Count:    count,
		Apiaries: apiaries,
	}

	if err = e.responseWriter(w, res); err != nil {
		_ = ErrInternal.WriteToResponse(w)
	}
}

func (e *AccountApiariesServiceEndpoints) mappingServiceError(err error) (httpError *Error) {
	if errors.Is(err, internal.ErrApiaryNotFound) {
		return NewError(http.StatusNotFound, err.Error())
	}

	if errors.Is(err, internal.ErrApiaryIsAlreadyBinded) {
		return NewError(http.StatusConflict, err.Error())
	}

	if errors.Is(err, internal.ErrBadQRCodeContent) {
		return NewError(http.StatusBadRequest, err.Error())
	}

	return nil
}
