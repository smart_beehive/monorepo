package http

import (
	"errors"
	"net/http"

	"bitbucket.org/smart_beehive/backend/internal/models"
	"github.com/gorilla/mux"
	"github.com/tomasen/realip"
)

type ClientClaimsPassingToContext struct {
	next http.Handler
}

func MakePassingClientClaimsToRequestContext() mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return &ClientClaimsPassingToContext{
			next: next,
		}
	}
}

func (p *ClientClaimsPassingToContext) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	clientClaims, err := models.NewClientClaims(realip.FromRequest(r), r.UserAgent())
	if procHandleErrorFromService(w, err, p.mappingServiceError) {
		return
	}

	r = r.WithContext(clientClaims.ToCtx(r.Context()))

	p.next.ServeHTTP(w, r)
}

func (p *ClientClaimsPassingToContext) mappingServiceError(err error) (httpError *Error) {
	if errors.Is(err, models.ErrIPAndUserAgentMustNotBeEmpty) {
		return NewErrorWithErrMsg(http.StatusBadRequest, err)
	}

	return nil
}
