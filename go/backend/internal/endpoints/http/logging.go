package http

import (
	"net/http"
	"time"

	"github.com/felixge/httpsnoop"
	"github.com/go-logr/logr"
	"github.com/gorilla/mux"
)

type responseLogger struct {
	w http.ResponseWriter

	status int
	size   int
}

func (l *responseLogger) Write(b []byte) (int, error) {
	size, err := l.w.Write(b)
	l.size += size

	return size, err
}

func (l *responseLogger) WriteHeader(s int) {
	l.w.WriteHeader(s)
	l.status = s
}

func newResponseLogger(w http.ResponseWriter) (*responseLogger, http.ResponseWriter) {
	logger := &responseLogger{w: w, status: http.StatusOK}
	return logger, httpsnoop.Wrap(w, httpsnoop.Hooks{
		Write: func(httpsnoop.WriteFunc) httpsnoop.WriteFunc {
			return logger.Write
		},
		WriteHeader: func(httpsnoop.WriteHeaderFunc) httpsnoop.WriteHeaderFunc {
			return logger.WriteHeader
		},
	})
}

func MakeLogging(l logr.Logger) mux.MiddlewareFunc {
	l = l.WithName("HTTP Log")

	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			startTime := time.Now()
			url := r.URL.String()

			l.Info("Request",
				"ts", startTime.String(),
				"method", r.Method,
				"url", url,
			)

			responseLogger, w := newResponseLogger(w)

			next.ServeHTTP(w, r)

			if r.MultipartForm != nil {
				r.MultipartForm.RemoveAll()
			}

			l.Info("Response",
				"pt", time.Since(startTime).String(),
				"method", r.Method,
				"url", url,
				"content-length", responseLogger.size,
				"status", responseLogger.status,
			)
		})
	}
}
