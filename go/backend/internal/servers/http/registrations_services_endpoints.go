package http

import (
	"net/http"

	endpoints "bitbucket.org/smart_beehive/backend/internal/endpoints/http"
)

func RegistrationAuthServiceEndpoints(s *Server, endpoints *endpoints.AuthServiceEndpoints) {
	router := s.router.PathPrefix("/api/auth").Subrouter()

	router.HandleFunc("/connect/{provider}", endpoints.ConnectProvider)
	router.HandleFunc("/callback_from_provider", endpoints.CallbackFromProvider)
	router.HandleFunc("/token", endpoints.Token)
	router.HandleFunc("/refresh", endpoints.Refresh)
	router.HandleFunc("/sign_out", endpoints.SignOut)
}

func RegistrationAccountApiariesServiceEndpoints(s *Server, endpoints *endpoints.AccountApiariesServiceEndpoints) {
	router := s.router.PathPrefix("/api/account/apiaries").Subrouter()

	router.HandleFunc("/bind", endpoints.Bind).Methods(http.MethodPost)
	router.HandleFunc("/rename/{id}", endpoints.Rename).Methods(http.MethodPut)
	router.HandleFunc("/rebind-beehives/{id}/{newBindApiaryID}", endpoints.RebindBeeHeevies).Methods(http.MethodPost)
	router.HandleFunc("/unbind-beehives/{id}", endpoints.RebindBeeHeevies).Methods(http.MethodDelete)
	router.HandleFunc("/unbind/{id}", endpoints.Unbind).Methods(http.MethodDelete)
	router.HandleFunc("/read/{id}", endpoints.Read).Methods(http.MethodGet)
	router.HandleFunc("/read-page", endpoints.ReadPage).
		Queries("pageNumber", "{pageNumber:[0-9]+}").
		Methods(http.MethodGet)
}

func RegistrationAccountBeeHiveServiceEndpoints(s *Server, endpoints *endpoints.AccountBeeHivesServiceEndpoints) {
	router := s.router.PathPrefix("/api/account/beehives").Subrouter()

	router.HandleFunc("/bind", endpoints.Bind).Methods(http.MethodPost)
	router.HandleFunc("/rebind/{id}", endpoints.Rebind).Methods(http.MethodPost)
	router.HandleFunc("/unbind/{id}", endpoints.Unbind).Methods(http.MethodDelete)
	router.HandleFunc("/read/{id}", endpoints.Read).Methods(http.MethodGet)
	router.HandleFunc("/read-page", endpoints.ReadPage).
		Queries("pageNumber", "{pageNumber:[0-9]+}").
		Queries("bindApiaryID", "{bindApiaryID}").
		Methods(http.MethodGet)
}
