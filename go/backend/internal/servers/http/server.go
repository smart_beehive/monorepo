package http

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/go-logr/logr"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

type ServerConfig struct {
	CORS CORS

	ListenAddress     string        `split_words:"true" default:"0.0.0.0:80" desc:"адрес прослушивания сервером."`
	ReadTimeout       time.Duration `split_words:"true" default:"15s" desc:"максимальная продолжительность чтения всего запроса, включая его тело."`
	ReadHeaderTimeout time.Duration `split_words:"true" default:"0s" desc:"сколько времени требуется для чтения заголовков запросов."`
	WriteTimeout      time.Duration `split_words:"true" default:"15s" desc:"максимальная продолжительность до тайм-аута записи ответа."`
	IdleTimeout       time.Duration `split_words:"true" default:"60s" desc:"максимальное время ожидания следующего запроса при включении keepalives."`
	MaxHeaderBytes    int           `split_words:"true" default:"0" desc:"максимальное количество байт, которое сервер будет читать, анализируя ключи и значения заголовка запроса, включая строку запроса."`
}

type CORS struct {
	Headers []string `split_words:"true" default:"Accept,Accept-Language,Content-Language,Origin" desc:"принимаются только те запросы, которые используют содержат определенные заголовки."`
	Methods []string `split_words:"true" default:"GET,HEAD,POST" desc:" принимаются запросы только определенного типа."`
	Origins []string `split_words:"true" default:"*" desc:"принимаются запросы только с определенных адресов."`
}

type Server struct {
	base   *http.Server
	router *mux.Router
}

func NewServer(config ServerConfig) *Server {
	router := mux.NewRouter()

	router.Use(handlers.CORS(
		handlers.AllowedHeaders(config.CORS.Headers),
		handlers.AllowedMethods(config.CORS.Methods),
		handlers.AllowedOrigins(config.CORS.Origins),
		handlers.AllowCredentials(),
	))

	return &Server{
		base: &http.Server{
			Addr:              config.ListenAddress,
			Handler:           router,
			ReadTimeout:       config.ReadTimeout,
			ReadHeaderTimeout: config.ReadHeaderTimeout,
			WriteTimeout:      config.WriteTimeout,
			IdleTimeout:       config.IdleTimeout,
			MaxHeaderBytes:    config.MaxHeaderBytes,
		},
		router: router,
	}
}

func (s *Server) LogRouter(l logr.Logger) (err error) {
	err = s.router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		keysvals := []interface{}{}

		pathTemplate, err := route.GetPathTemplate()
		if err == nil {
			keysvals = append(keysvals, "path template", pathTemplate)
		}

		methods, err := route.GetMethods()
		if err == nil {
			keysvals = append(keysvals, "methods", strings.Join(methods, ","))
		}

		l.Info("server router", keysvals...)

		return nil
	})
	if err != nil {
		return fmt.Errorf("s.router.Walk: %w", err)
	}

	return nil
}

func (s *Server) UseMWS(mws ...mux.MiddlewareFunc) {
	s.router.Use(mws...)
}

func (s *Server) ListenAndServe(ctx context.Context) error {
	errCh := make(chan error)

	go func() {
		errCh <- s.base.ListenAndServe()
	}()

	select {
	case err := <-errCh:
		return err
	case <-ctx.Done():
		s.base.Shutdown(context.Background())

		return ctx.Err()
	}
}
