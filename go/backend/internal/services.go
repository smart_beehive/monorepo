package internal

import (
	"context"
	"errors"

	"bitbucket.org/smart_beehive/backend/internal/models"
)

var (
	ErrAccessDonied     = errors.New("access denied")
	ErrBadQRCodeContent = errors.New("bad qr code content")
)

var (
	ErrAuthProviderNotSupported      = errors.New("auth provider not supported")
	ErrOAuthConnectSessionNotExists  = errors.New("oauth session not exists")
	ErrOAuthConnectSessionExpired    = errors.New("oauth connect session expired")
	ErrExchangeCodeForTokenNotExists = errors.New("exchange code for token not exists")
	ErrExchangeCodeForTokenExpired   = errors.New("exchange code for token expired")
	ErrAuthTokenNotExists            = errors.New("auth token not exists")
	ErrRefreshTokenInvalid           = errors.New("refresh token invalid")
	ErrAuthTokenInvalid              = errors.New("auth token invalid")
)

const (
	RedirectURLToTokenValueCodeName = "internalCode"
)

// AuthService сервис авторизации/регистрации пользователей.
type AuthService interface {
	// ConnectProvider метод подключения внешнего провайдера авторизации.
	ConnectProvider(ctx context.Context, providerName models.OAuthProviderName) (redirectURLToProvider string, err error)

	// CallbackFromProvider метод обрабатывает обратный вызов от внешнего провайдера авторизации.
	CallbackFromProvider(ctx context.Context, code, state string) (redirectURLToToken string, err error)

	// Token получение токена по внутренему коду CallbackFromProvider.
	Token(ctx context.Context, internalCode string) (token *models.AuthToken, err error)

	// Refresh перевыпуск токенов.
	Refresh(ctx context.Context, refreshToken string) (token *models.AuthToken, err error)

	// SignOut завершение сессии токена.
	SignOut(ctx context.Context) error
}

type AuthServiceMW func(next AuthService) AuthService

func WrapAuthServiceMW(s AuthService, mws ...AuthServiceMW) AuthService {
	for _, mw := range mws {
		s = mw(s)
	}

	return s
}

var (
	ErrApiaryNotFound                    = errors.New("apiary not found")
	ErrApiaryIsAlreadyBinded             = errors.New("apiary is already binded")
	ErrApiaryNotUnbindMustRebindBeehives = errors.New("apiary not unbind, must rebind beehives")
)

// AccountApiariesService сервис управляющий пасеками аккаунта.
type AccountApiariesService interface {
	// Bind метод обрабатывает добавление пасеки к аккаунту пользователя.
	//
	// На каждой пасеке есть QR код в котором находится зашифрованный идентификатор пасеки.
	Bind(ctx context.Context, qrCodeContent string, name string) (apiary *models.BindedApiary, err error)

	// Rename переименовать пасеку.
	Rename(ctx context.Context, id string, name string) (err error)

	// RebindBeeHeevies метод переносит все ульи на другую пасеку.
	RebindBeeHives(ctx context.Context, id, newBindApiary string) (err error)

	// UnbindBeeHives отвязывает все ульи от пасеки.
	UnbindBeeHives(ctx context.Context, id string) (err error)

	// Unbind метод обрабатывает удаление пасеки из аккаунта пользователя.
	Unbind(ctx context.Context, id string) (err error)

	// Read метод возвращает данные об пасеке.
	Read(ctx context.Context, id string) (beehive *models.BindedApiary, err error)

	// ReadPage метод возвращает страницу с данными об пасеках, где count это общее кол-во записей.
	ReadPage(ctx context.Context, pageNumber int) (count int, apiaries []*models.BindedApiary, err error)
}

var (
	ErrBeeHiveNotFound        = errors.New("beehive not found")
	ErrBeeHiveIsAlreadyBinded = errors.New("beehive is already binded")
)

// AccountBeeHivesService сервис управляющий ульями аккаунта.
type AccountBeeHivesService interface {
	// Bind метод обрабатывает добавление улия в пасеку к аккаунту пользователя.
	//
	// На каждом улье есть QR код в котором находится идентификатор улья.
	Bind(ctx context.Context, qrCodeContent, bindApiaryID string) (beehive *models.BindedBeeHive, err error)

	// Rebind метод выполняет передобавление улья в другую пасеку аккаунта пользователя.
	Rebind(ctx context.Context, id, newBindApiaryID string) (beehive *models.BindedBeeHive, err error)

	// Unbind метод обрабатывает удаление улья из пасеки из аккаунта пользователя.
	Unbind(ctx context.Context, id string) (err error)

	// Read метод возвращает данные об улье.
	Read(ctx context.Context, id string) (beehive *models.BindedBeeHive, err error)

	// ReadPage метод возвращает страницу с данными об улье, где count общее кол-во записей.
	ReadPage(ctx context.Context, bindApiaryID string, pageNumber int) (count int, beehives []*models.BindedBeeHive, err error)
}
